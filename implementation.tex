%!TEX root=DBCompress.tex


\section{Compression and Decompression}\label{sec_implementation}

In this section, we discuss how we can use arithmetic coding correctly for compression and decompression given a Bayesian Network. In Section~\ref{sec_compression}, we discuss implementation details that ensure the correctness of arithmetic coding using finite precision float numbers. \techreport{Section~\ref{sec_delta} talks about an algorithm that we borrow from prior work for additional compression. }In Section~\ref{sec_decompression} we describe the decompression algorithm. %\paper{In \system, we also used the Delta Coding technique~\cite{raman2006wring} from prior work for additional compression, the details can be found in our technical report~\cite{}.} 


\subsection{Compression}\label{sec_compression}

We use the same notation as in Section~\ref{sec_model_learning}: a tuple $t$ contains $m$ attributes, and without loss of generality we assume that they follow the topological order of the Bayesian network:
$$ t = \{a_{1}, \ldots, a_{m}\}, \textit{parent}(a_{j}) \subseteq \{a_{1}, \ldots, a_{j-1}\} $$

We first compute a probability interval for each branch in a \probtree. For each \probtree~$T$, we define $\text{PI}_T$ as a mapping from branches of $T$ to probability intervals. The definition is similar to the one in Section~\ref{sec_arithmetic}: let $v$ be any non-leaf node in $T$, suppose $v$ has $k$ children $u_1, \ldots, u_k$, and the edge between $v$ and $u_i$ is associated with probability $p_i$, then $\text{PI}_T(v \rightarrow u_i)$ is defined as:
$$ \text{PI}(v \rightarrow u_i) = [\sum_{j < i} p_j, \sum_{j \leq i} p_j] $$

Now we can compute the probability interval of $t$. Let $T_j$ be the \probtree~for $a_j$ conditioned on its parent attributes. Denote the leaf node in $T_j$ that $a_j$ corresponds to as $v_j$. Suppose the path from the root of $T_j$ to $v_j$ is $u_{j1} \rightarrow u_{j2} \rightarrow \ldots \rightarrow u_{jk_j} \rightarrow v_j$. Then, the probability interval of tuple $t$ is:
\begin{align*}
[L,R] = & \text{PI}_{T_1}(u_{11} \rightarrow u_{12}) \circ \ldots \circ \text{PI}_{T_1}(u_{1k_1} \rightarrow v_1) \circ \\
	    & \text{PI}_{T_2}(u_{21} \rightarrow u_{22}) \circ \ldots \circ \text{PI}_{T_2}(u_{2k_2} \rightarrow v_2) \circ \ldots \circ \\
	    & \text{PI}_{T_m}(u_{m1} \rightarrow u_{m2}) \circ \ldots \circ \text{PI}_{T_m}(u_{mk_m} \rightarrow v_m)
\end{align*}
where $\circ$ is the probability interval multiplication operator defined in Section~\ref{sec_arithmetic}. The code string of tuple $\mathbf{t}$ corresponds to the largest subinterval of $[L,R]$ of the form $[2^{-k}M, 2^{-k}(M+1)]$ as described in Section~\ref{sec_arithmetic}.

In practice, we cannot directly compute the final probability interval of a tuple: there could be hundreds of probability intervals in the product, so the result can easily exceed the precision limit of a floating-point number. 

Algorithm~\ref{alg_encode} shows the pseudo-code of the precision-aware compression algorithm. We leverage two tricks to deal with the finite precision problem: the classic early bits emission trick~\cite{langdon1984introduction} is described in Section~\ref{sec_early_bit_emission}; the new deterministic approximation trick is described in Section~\ref{sec_approx}.

\begin{algorithm}
	\paper{
	\scriptsize
	}
	\caption{Encoding Algorithm}
	\label{alg_encode}
	\begin{algorithmic}
		\Function{ArithmeticCoding}{$[l_1,r_1], \ldots, [l_n,r_n]$}
			\State $\text{code} \leftarrow \emptyset$
			\State $I_t \leftarrow [0, 1]$
			\For{$i = 1$ to $n$}
				\State $I_t \leftarrow I_t \diamond [l_i, r_i]$
				\While{$\exists k=0 \text{ or } 1, I_t \subseteq [\frac{k}{2}, \frac{k+1}{2}]$}
					\State $\text{code} \leftarrow \text{code} + k$
					\State $I_t \leftarrow [2I_t.l - k, 2I_t.r - k]$
				\EndWhile
			\EndFor
			\State Find smallest $k$ such that \\ \qquad \qquad $\exists M, [2^{-k}M, 2^{-k}(M+1)] \subseteq I_t$
			\State \Return $\text{code} + M$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

\vspace{-10pt}

\subsubsection{Early Bits Emission}\label{sec_early_bit_emission}

Without loss of generality, suppose
$$ [L,R] = [l_1, r_1] \circ [l_2, r_2] \circ \ldots \circ [l_n, r_n] $$
Define $[L_i,R_i]$ as the product of first $i$ probability intervals:
$$ [L_i,R_i] = [l_1, r_1] \circ [l_2, r_2] \circ \ldots \circ [l_i, r_i] $$
If there exist positive integer $k_i$ and non-negative integer $M_i$ such that
$$ 2^{-k_i} M_i \leq L_i < R_i \leq 2^{-k_i} (M_i + 1) $$
Then the first $k_i$ bits of the code string of $t$ must be the binary representation of $M_i$. Define 
$$[L'_i, R'_i] = [2^{k_i} L_i - M_i, 2^{k_i} R_i - M_i] $$
Then it can be verified that
\begin{align*}
& \textit{binary\_code}([L,R]) = \textit{binary\_code}(M_i) +\\
							   & \textit{binary\_code}([L'_i,R'_i] \circ [l_{i+1}, r_{i+1}] \ldots \circ [l_n,r_n])
\end{align*} 
Therefore, we can immediately output the first $k_i$ bits of the code string. After that, we compute the product:
$$ [L'_i,R'_i] \circ [l_{i+1}, r_{i+1}] \ldots \circ [l_n,r_n] $$
We can recursively use the same early bit emitting scheme for this product. In this way, we can greatly reduce the likelihood of precision overflow.

\subsubsection{Deterministic Approximation}\label{sec_approx}

For probability intervals containing $0.5$, we cannot emit any bits early. In rare cases, such a probability interval would exceed the precision limit, and the correctness of our algorithm would be compromised. 

To address this problem, we introduce the deterministic approximation trick.
Recall that the correctness of arithmetic coding relies on the non-overlapping property of the probability intervals. Therefore, we do not need to compute probability intervals with perfect accuracy: the correctness is guaranteed as long as we ensure these probability intervals do not overlap with each other.

Formally, let $t_1, t_2$ be two different tuples, and suppose their probability intervals are:
\begin{align*}
\text{PI}(t_1) = [l_1, r_1] = [l_{11}, r_{11}] \circ [l_{12}, r_{12}] \circ \ldots \circ [l_{1n_1}, r_{1n_1}]\\
\text{PI}(t_2) = [l_2, r_2] = [l_{21}, r_{21}] \circ [l_{22}, r_{22}] \circ \ldots \circ [l_{2n_2}, r_{2n_2}]
\end{align*}

The deterministic approximation trick is to replace $\circ$ operator with a deterministic operator $\diamond$ that approximates $\circ$ and has the following properties:
\begin{itemize}
	\item
	For any two probability intervals $[a,b]$ and $[c,d]$:
	$$ [a,b] \diamond [c,d] \subseteq [a,b] \circ [c,d] $$
	\item
	For any two probability intervals $[a,b]$ and $[c,d]$ with $b - a \geq \epsilon$ and $d - c \geq \epsilon$. Let $[l,r] = [a,b] \diamond [c,d]$, then:
	$$ \exists k, M, 2^{-k} M \leq l < r \leq 2^{-k} (M+1), 2^k(r - l) \geq \epsilon $$
\end{itemize}

In other words, the product computed by $\diamond$ operator is always a subset of the product computed by $\circ$ operator, and $\diamond$ operator always ensures that the product probability interval has length greater than or equal to $\epsilon$ after emitting bits. The first property guarantees the non-overlapping property still holds, and the second property prevents potential precision overflow. As we will see in Section~\ref{sec_decompression}, these two properties are sufficient to guarantee the correctness of arithmetic coding.

\techreport{
\subsection{Delta Coding}\label{sec_delta}
Notice that our compression scheme thus far has focused on compressing ``horizontally'', i.e., reducing the size of each tuple, independent of each other.
In addition to this, we could also compress ``vertically'', where
we compress tuples relative to each other. For this, we directly leverage an algorithm developed in prior work.
%\agp{Does it make sense to describe this earlier? Or no? For example, in the workflow?}
Raman and Swart~\cite{raman2006wring} developed an optimal method for compressing a set of binary code strings. This coding scheme (called ``Delta Coding'' in their paper) achieves $O(n \log n)$ space saving where $n$ is the number of code strings. For completeness, the pseudo-code of a variant of their method that is used in our system is listed in Algorithm~\ref{alg_delta}.

\begin{algorithm}
	%\scriptsize
	\caption{Delta Coding}
	\label{alg_delta}
	\begin{algorithmic}
		\Function{DeltaCoding}{$s_1, \ldots, s_n$}
			\State // $s_1, \ldots, s_n$ are binary codes of $t_1, \ldots, t_n$
			\State Sort $s_1, \ldots, s_n$
			\State $l \leftarrow \lfloor \log n \rfloor$
			\State If $len(s_i) < l$, pad $s_i$ with trailing zeros
			\State Let $s_i = a_i b_i$ where $a_i$ is $l$-bit prefix of $s_i$
			\State $s'_i \leftarrow Unary(a_i - a_{i-1}) + b_i$
			\State \Return $\{s'_1, \ldots, s'_n\}$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

In Algorithm~\ref{alg_delta}, $Unary(s)$ is the unary code of $s$ (i.e., $0 \rightarrow 0$, $1 \rightarrow 10$, $2 \rightarrow 110$, etc.). Delta Coding replaces the $\lfloor \log n \rfloor$-bit prefix of each tuple by an unary code with at most $2$ bits on average. Thus it saves about $n (\log_2 n - 2)$ bits storage space in total.
}

\subsection{Decompression}\label{sec_decompression}

When decompressing, \system~first reads in the dataset schema and all of the model information, and stores them in the main memory. After that, it scans over the compressed dataset, extracts and decodes the binary code strings to recover the original tuples.

\begin{algorithm}
	\paper{
	\scriptsize
	}
	\caption{Decoding Algorithm}
	\label{alg_decode}
	\begin{algorithmic}
		\Function{Decoder.Initialization}{}
			\State $I_b \leftarrow [0, 1]$
			\State $I_t \leftarrow [0, 1]$
		\EndFunction
		\Function{Decoder.GetNextBranch}{$branches$}
			\While{\textbf{not} $\exists br \in branches, I_b \subseteq I_t \diamond \text{PI}(br)$}
				\State Read in the next bit $x$
				\State $I_b \leftarrow I_b \circ [\frac{x}{2}, \frac{x+1}{2}]$
			\EndWhile
		\If{$I_b \subseteq I_t \diamond \text{PI}(br), br \in branches$}
			\State $I_t \leftarrow I_t \diamond \text{PI}(br)$
			\While{$\exists k=0 \text{ or } 1, I_t \subseteq [\frac{k}{2}, \frac{k+1}{2}]$}
				\State $I_t \leftarrow [2I_t.l - k, 2I_t.r - k]$
				\State $I_b \leftarrow [2I_b.r - k, 2I_b.r - k]$
			\EndWhile
			\State \Return $br$
		\EndIf
		\EndFunction
	\end{algorithmic}
\end{algorithm}

Algorithm~\ref{alg_decode} describes the procedure to decide the next branch. \techreport{Combined with decompression function in Algorithm~\ref{alg_api_calls}, we get the complete algorithm for decoding. 

}The decoder maintains two probability intervals $I_b$ and $I_t$. $I_b$ is the probability interval corresponding to all the bits that the algorithm has read in so far. $I_t$ is the probability interval corresponding to all decoded attributes. At each step, the algorithm computes the product of $I_t$ and the probability interval for every possible attribute value, and then checks whether $I_b$ is contained by one of those probability intervals. If so, we can decide the next branch, and update $I_t$ accordingly. If not, we continue reading in the next bit and update $I_b$.

\paper{By calling Algorithm~\ref{alg_decode} repeatedly, we can gradually decode the whole tuple. The full decoding procedure with an illustrative example can be found in our technical report~\cite{techreport}.}

\techreport{
As an illustration of the behavior of Algorithm~\ref{alg_decode}, Table~\ref{tbl_decode_example} shows the step by step execution for the following example:
$$ t = (a_1, a_2, a_3), [l,r] = [\frac{1}{3},\frac{1}{2}] \circ [\frac{1}{4}, \frac{1}{2}] \circ [\frac{1}{2}, \frac{2}{3}], \text{code} = 01100110 $$
\begin{table}[h]
\small
\vspace{-5pt}
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Step & $I_t$ & $I_b$ & Input & Output \\
		\hline
		1 & $[0,1]$ & $[0,1]$ & & \\
		2 & $[0,1]$ & $[0,\frac{1}{2}]$ & 0 &\\
		3 & $[0,1]$ & $[\frac{1}{4},\frac{1}{2}]$ & 01 &\\
		4 & $[\frac{1}{3},\frac{1}{2}]$ & $[\frac{1}{4},\frac{1}{2}]$ & 01 & $a_1$ \\
		5 & $[\frac{2}{3},1]$ & $[\frac{1}{2},1]$ & 01 & $a_1$ \\
		6 & $[\frac{1}{3},1]$ & $[0,1]$ & 01 & $a_1$ \\	
		7 & $[\frac{1}{3},1]$ & $[\frac{1}{2},1]$ & 011 & $a_1$ \\
		8 & $[\frac{1}{3},1]$ & $[\frac{1}{2},\frac{3}{4}]$ & 0110 & $a_1$ \\
		9 & $[\frac{1}{3},1]$ & $[\frac{1}{2},\frac{5}{8}]$ & 01100 & $a_1$ \\
		10 & $[\frac{1}{2},\frac{2}{3}]$ & $[\frac{1}{2},\frac{5}{8}]$ & 01100 & $a_1,a_2$ \\
		11 & $[0,\frac{1}{3}]$ & $[0,\frac{1}{4}]$ & 01100 & $a_1,a_2$ \\
		12 & $[0,\frac{2}{3}]$ & $[0,\frac{1}{2}]$ & 01100 & $a_1,a_2$ \\
		13 & $[0,\frac{2}{3}]$ & $[\frac{1}{4},\frac{1}{2}]$ & 011001 & $a_1,a_2$ \\
		14 & $[0,\frac{2}{3}]$ & $[\frac{3}{8},\frac{1}{2}]$ & 0110011 & $a_1,a_2$ \\
		15 & $[0,\frac{2}{3}]$ & $[\frac{3}{8},\frac{7}{16}]$ & 01100110 & $a_1,a_2$ \\
		16 & $[\frac{1}{3},\frac{4}{9}]$ & $[\frac{3}{8},\frac{7}{16}]$ & 01100110 & $a_1,a_2,a_3$ \\
		17 & $[\frac{2}{3},\frac{8}{9}]$ & $[\frac{3}{4},\frac{7}{8}]$ & 01100110 & $a_1,a_2,a_3$ \\
		18 & $[\frac{1}{3},\frac{7}{9}]$ & $[\frac{1}{2},\frac{3}{4}]$ & 01100110 & $a_1,a_2,a_3$ \\
		\hline
	\end{tabular}
	\caption{Decoding Example}\label{tbl_decode_example}
	\vspace{-5pt}
\end{table}
}

Notice that Algorithm~\ref{alg_decode} mirrors Algorithm~\ref{alg_encode} in the way it computes probability interval products. This design is to ensure that the encoding and decoding algorithm always apply the same deterministic approximation that we described in Section~\ref{sec_approx}. The following theorem states the correctness of the algorithm (the proof can be found in \techreport{the appendix}\paper{our technical report~\cite{techreport}}):

\begin{theorem}\label{thm_decode}
	Let $[l_1,r_1],\ldots,[l_n,r_n]$ be probability intervals with $r_i - l_i \geq \epsilon$ where $\epsilon$ is the small constant defined in Section~\ref{sec_approx}. Let $s$ be the output of Algorithm~\ref{alg_encode} on these probability intervals.
	Then Algorithm~\ref{alg_decode} can always determine the correct branch from alternatives using $s$ as input:
	$$ \text{PI}(\text{Decoder.GetNextBranch}(branch_i)) = [l_i, r_i] $$
\end{theorem}