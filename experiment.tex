%!TEX root=DBCompress.tex

\section{Experiments}\label{sec_experiment}

In this section, we evaluate the performance of \system~against the state of the art semantic compression algorithms SPARTAN~\cite{babu2001spartan} and ItCompress~\cite{jagadish2004itcompress} (see Table~\ref{tbl_intro}). For reference we also include the performance of gzip~\cite{ziv1977universal}, a well-known syntactic compression algorithm. 

We use the following four publicly available datasets:
\begin{itemize}
\item
\textit{Corel} (http://kdd.ics.uci.edu/databases/CorelFeatures) is a 20 MB dataset containing 68,040 tuples with 32 numerical color histogram attributes.
\item
\textit{Forest-Cover} (http://kdd.ics.uci.edu/databases/covertype) is a 75 MB dataset containing 581,000 tuples with $10$ numerical and $44$ categorical attributes.
\item
\textit{Census} (http://thedataweb.rm.census.gov/ftp/cps\_ftp.html) is a 610MB dataset containing 676,000 tuples with $36$ numerical and $332$ categorical attributes.
\item
\textit{Genomes} (ftp://ftp.1000genomes.ebi.ac.uk) is a 18.2GB dataset containing 1,832,506 tuples with about $10$ numerical and $2500$ categorical attributes.\footnote{In this dataset, many attributes are optional and these numbers indicate the average number of attributes that appear in each tuple.}
\end{itemize}

The first three datasets have been used in previous papers~\cite{babu2001spartan, jagadish2004itcompress}, and the compression ratio achieved by SPARTAN, ItCompress and gzip on these datasets have been reported in Jagadish et al.'s work~\cite{jagadish2004itcompress}. We did not reproduce these numbers and only used their reported performance numbers for comparison. For the \textit{Census} dataset, the previous papers only used a subset of the attributes in the experiments ($7$ categorical attributes and $7$ numerical attributes). Since we are unaware of the selection criteria of the attributes, we are unable to compare with their algorithms, and we will only report the comparison with gzip.

For the \textit{Corel} and \textit{Forest-Cover} datasets, we set the error tolerance as a percentage ($1\%$ by default) of the width of the range for numerical attributes as in previous work. For the \textit{Census} dataset, we set all error tolerance to $0$ (i.e. the compression is lossless). For the \textit{Genomes} dataset, we set the error tolerance for integer attributes to $0$ and float number attributes to $10^{-8}$.

In all experiments, we only used the first 2000 tuples in the structure learning algorithm \techreport{(Algorithm~\ref{alg_structure_learning}) }to improve efficiency. All available tuples are used in other parts of the algorithm.

\techreport{
	We remark that unlike experiments in machine learning, there is no training/test split of the dataset in our setting. We always use all the available data to train the model, and the goal is simply to minimize the size of the compressed dataset. There is no concept of overfitting in this scenario.
}

\subsection{Compression Rate Comparison}

Figure~\ref{fig_ratio} shows the comparison of compression rate on the \textit{Corel} and \textit{Forest-Cover} datasets. In these figures, X axis is the error tolerance for numerical attributes (\% of the width of range), and Y axis is the compression ratio, defined as follows:
$$ \text{compression ratio} = \frac{\text{data size with compression}}{\text{data size without compression}} $$

As we can see from the figures, \system~significantly outperforms the other algorithms. When the error tolerance threshold is small (0.5\%), \system~achieves about {\em 50\% reduction in compression ratio on the Forest Cover dataset and 75\% reduction on the Corel dataset, compared to the nearest competitor ItCompress (gzip)}, which applies gzip algorithm on top of the result of ItCompress. The benefit of not using gzip as a post-processing step is that we can still permit tuple-level access without decompressing a larger unit.

The remarkable superiority of our system in the Corel dataset reflects the advantage of \system~in compressing numerical attributes. Numerical attribute compression is known to be a hard problem~\cite{roth1993database} and none of the previous systems have effectively addressed it. In contrast, our encoding scheme can leverage the skewness of the distribution and achieve near-optimal performance.

\begin{figure}[h]
	\centering
	
	\subfigure[Forest Cover]{
		\includegraphics[width = 6cm]{forest_cover.png}
		\vspace{-5pt}
		\label{fig_ratio_forest_cover}
	}
	\subfigure[Corel]{
		\includegraphics[width = 6cm]{corel.png}
		\vspace{-5pt}
		\label{fig_ratio_corel}
	}
	\vspace{-10pt}	
	\caption{Error Threshold vs Compression Ratio}\label{fig_ratio}
	%\vspace{-10pt}
\end{figure}

Figure~\ref{fig_ratio_lossless} shows the comparison of compression rate on the \textit{Census} and \textit{Genomes} datasets. Note that in these two datasets, we set the error tolerance threshold to be extremely small, so that the compression is essentially lossless. As we can see, even in the lossless compression scenario, our algorithm still outperforms gzip significantly. {\em Compared to gzip, \system~achieves $48\%$ reduction in compression ratio in Census dataset and $56\%$ reduction in Genomes dataset.}

\begin{figure}[h]
	\centering
	\includegraphics[width = 6cm]{bar_lossless.png}
	\vspace{-10pt}	
	\caption{Compression Ratio Comparison}\label{fig_ratio_lossless}
	\vspace{-10pt}	
\end{figure}

\subsection{Compression Breakdown}

As we have seen in the last section, \system~achieved superior compression ratio in all four datasets. In this section, we use detailed case studies to illustrate the reason behind the significant improvement over previous papers.

\subsubsection{Categorical Attributes}

In this section, we study the source of the compression in \system~for categorical attributes. We will use three different treatments for the categorical attributes and see how much compression is achieved for each of these treatments: 
\begin{itemize} \setlength{\itemsep}{0ex} \setlength{\topsep}{0ex}
	\item
	Domain Code: We replace the categorical attribute values with short binary code strings. Each code string has length $\lceil \log_2 N \rceil$, where $N$ is the total number of possible categorical values for the attribute. 
	\item
	Column: We ignore the correlations between categorical attributes and treat all the categorical attributes as independent. 
	\item
	Full: We use both the correlations between attributes and the skewness of attribute values in our compression algorithm.
\end{itemize}

We will use the \textit{Genomes} and \textit{Census} dataset here since they consist of mostly categorical attributes. We keep the compression algorithm for numerical attributes unchanged in all treatments. Figure~\ref{fig_ratio_categorical} shows the compression ratio of the three treatments:

\begin{figure}[h]
	\centering
	\vspace{-10pt}	
	\includegraphics[width = 5cm]{bar_categorical.png}
	\vspace{-10pt}	
	\caption{Compression Ratio Comparison}\label{fig_ratio_categorical}
	\vspace{-10pt}	
\end{figure}

As we can see, the compression ratio of the basic domain coding scheme can be improved up to $70$\% if we take into account the skewness of the distribution in attribute values. Furthermore, the correlation between attributes is another opportunity for compression, which improved the compression ratio by $50\%$ in both datasets.

An interesting observation is that the Column treatment achieves comparable compression ratio as gzip in both datasets, which suggests that gzip is in general capable of capturing the skewness of distribution for categorical attributes, but unable to capture the correlation between attributes.

\subsubsection{Numerical Attributes}

We now study the source of the compression in \system \\for numerical attributes. We use the following five treatments for the numerical attributes:
\begin{itemize} \setlength{\itemsep}{0ex} \setlength{\topsep}{0ex}
	\item
		IEEE Float: We use the IEEE Single Precision Floating Point standard to store all attributes.
	\item
		Discrete: Since all attributes in the dataset have value between $0$ and $1$, we use integer $i$ to represent a float number in range $[\frac{i}{10^7}, \frac{i + 1}{10^7}]$, and then store each integer using its 24-bit binary representation.
	\item
		Column: We ignore the correlation between numerical attributes and treat all attributes as independent.
	\item
		Full: We use both the correlations between attributes and distribution information about attribute values.
	\item
		Lossy: The same as the Full treatment, but we set the error tolerance at $10^{-4}$ instead.
\end{itemize}

We use the \textit{Corel} dataset here since it contains only numerical attributes. The error tolerance in all treatments except the last are set to be $10^{-7}$ to make sure the comparison is fair (IEEE single format has precision about $10^{-7}$). All the numerical attributes in this dataset are in range $[0, 1]$, with a distribution peaked at $0$. Figure~\ref{fig_ratio_numerical} shows the compression ratio of the five treatments.

\begin{figure}[h]
	\centering
	\vspace{-5pt}	
	\includegraphics[width = 5cm]{bar_numerical.png}
	\vspace{-10pt}	
	\caption{Compression Ratio Comparison}\label{fig_ratio_numerical}
	\vspace{-5pt}	
\end{figure}

As we can see, storing numerical attributes as float numbers instead of strings gives us about $55\%$ compression. However, the compression rate can be improved by another $50\%$ if we recognize distributional properties (i.e., range and skewness). Utilizing the correlation between attributes in the \textit{Corel} dataset only slightly improved the compression ratio by $3\%$. Finally, we see that the benefit of lossy compression is significant: even though we only reduced the precision from $10^{-7}$ to $10^{-4}$, the compression ratio has already been improved by $50\%$.

\subsection{Running Time}

\techreport{In this section we evaluate the running time of \system. Note that the time complexity of the compression and decompression components are both $O(n m)$, where $m$ is the number of attributes and $n$ is the number of tuples. In other words, the running time of these two components are linear to the size of the dataset. Therefore, the algorithm should scale well to large datasets in theory.	
}

Table~\ref{tbl_running_time} lists the running time of the five components in \system. All experiments are performed on a computer with eight\footnote{The implementation is single-threaded, so only one processor is used.} 3.4GHz Intel Xeon processors. For the \textit{Genomes} dataset, which contains $2500$ attributes---an extremely large number---we constructed the Bayesian Network manually. 
Note that none of the previous papers have been applied on a dataset with the magnitude of the \textit{Genomes} dataset (both in number of tuples and number of attributes). 

\techreport{
\textbf{Remark:} Due to a suboptimal implementation of the parameter tuning component in our current code, the actual time complexity of the parameter tuning component is $O(n m d)$ where $d$ is the depth of the Bayesian network. Therefore, Table~\ref{tbl_running_time} may not reflect the best possible running time of a fully optimized version of our compression algorithm.	
}
% Naturally, another approach would have been to apply structure learning to a smaller subset
% of tuples to speed it up; 
% instead, we used the simpler approach of constructing a straightforward Bayesian Network.
\begin{table}[h]
	\vspace{-15pt}
	\caption{Running Time of Different Components}\label{tbl_running_time}
	\centering
	\small
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		 & Forest Cov. & Corel & Census & Genomes\\
		\hline
		Struct. Learning & 5.5 sec & 2.5 sec & 20 min & N/A \\
		\hline
		Param. Tuning & 140 sec & 15 sec & 100 min & 40 min\\
		\hline
		Compression & 48 sec & 6 sec & 6 min & 50 min\\
		\hline
		Writing to File & 7 sec & 2 sec & 40 sec & 7 min\\
		\hline
		Decompression & 53 sec & 7.5 sec & 6 min & 50 min\\
		\hline
	\end{tabular}
	\vspace{-10pt}
\end{table}

%, the majority of time were spent on  For parameter tuning, it is possible to use only a subset of data. The only difference is that we need to assign a positive probability to every attribute value, even if it didn't appear in the training data. The real bottleneck is the arithmetic coding algorithm, which reflects the minimum possible running time using our algorithm.

As we can see from Table~\ref{tbl_running_time}, our compression algorithm scales reasonably: even with the largest dataset \textit{Genomes}, the compression can still be finished within hours. Recall that since our algorithm is designed for archival not online query processing, and {\em our goal is therefore to minimize storage as much as possible}, a few hours for large datasets is adequate.

The running time of the parameter tuning component can be greatly reduced if we use only a subset of tuples (as we did for structure learning). The only potential bottleneck is structure learning, which scales badly with respect to the number of attributes ($O(m^4)$). To handle datasets of this scale, another approach is to partition the dataset column-wise, and apply the compression algorithm on each partition separately. We plan to investigate this in future work.

%f the dataset has too many attributes, the scalability of structure learning algorithm might be an issue (recall that the structure learning algorithm scales as $O(m^4)$ where $m$ is the number of attributes). One way to deal with this issue is to manually construct Bayesian Network using domain knowledge, and another alternative is to partition the attributes into several groups and run the structure learning algorithm within each group separately.

We remark that, unlike gzip~\cite{ziv1977universal}, \system~allows random access of tuples without decompressing the whole dataset. Therefore, if users only need to access a few tuples in the dataset, then they will only need to decode those tuples, which would require far less time than decoding the whole dataset.
% One drawback of our compression algorithm is that the compression speed is relatively slow. On average, our compression algorithm is $10$ times slower than Lempel-Ziv~\cite{ziv1977universal}. However, as we will illustrate in the experiments, our algorithm can still compress $10$GB scale datasets within hours. Since our algorithm is primarily used for archival purpose, we will not compress and decompress datasets frequently. In this scenario, we believe that the storage saving achieved by our algorithm would significantly outweigh the trouble of spending a few more hours for compressing the dataset. 

% The decompression time is comparable with compression time. In both example datasets, the decompression time is about $50$ times slower than a linear scan of the raw (uncompressed) dataset. The running time is for a single processor, so utilizing multiple processors may improve the efficiency. Based on this result, we believe that it is possible to improve random access speed for database systems using our algorithm, but it is unlikely that the sequential scan rate can be improved. This result coincides with findings in prior work~\cite{babu2001spartan,jagadish2004itcompress,davies1999bayesian}: the compression \& decompression speed of all semantic compression schemes that exploits attribute correlations~\cite{babu2001spartan,jagadish2004itcompress,davies1999bayesian} are inevitably much slower than sequential scan rate.  That said, the focus of this paper (and prior semantic compression papers) is on reducing storage as much as possible for archival \& historical purposes.
%\agp{Can we say something positive about this? For example, something like:
%(a) This isn't that bad because the gains in compression outweigh the negatives?
%(b) Other schemes also have similar time performance.}

% \vspace{2mm}

% \noindent \textbf{Faster Compression \& Decompression}

% \vspace{2mm}

% Although the bisecting scheme described in Section~\ref{sec_example_model} is near-optimal, it requires computing at least one cumulative distribution function (CDF) value at each branch. In general, computing a CDF is costly, and our experiments suggest that this computation is in fact the bottleneck of compression and decompression.

% If we can avoid computing CDF at each branch, then it might be possible to further improve the efficiency of compression and decompression. One possible approach is to use approximations: instead of computing CDF explicitly, we can compute simpler function that approximates CDF. We plan to explore this in future work.

\subsection{Sensitivity to Bayesian Network Learning}

We now investigate the sensitivity of the performance of our algorithm with respect to the Bayesian network learning. We use the \textit{Census} dataset here since the correlation between attributes in this dataset is stronger than other datasets, so the quality of the Bayesian network can be directly reflected in the compression ratio.

Since our structure learning algorithm only uses a subset of the training data, one might question whether the selection of tuples in the structure learning component would affect the compression ratio. To test this, we run the algorithm for five times, and randomly choose the tuples participating in the structure learning. Table~\ref{tbl_sensitivity} shows the compression ratio of the five runs. As we can see, the variation between runs are insignificant, suggesting that our compression algorithm is robust.

\begin{table}[h]
	\vspace{-10pt}
	\caption{Sensitivity of the Structure Learning}\label{tbl_sensitivity}
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		No. of Exp. & 1 & 2 & 3 & 4 & 5\\
		\hline
		Comp. Ratio & 0.0460 & 0.0472 & 0.0471 & 0.0468  & 0.0476 \\
		\hline
	\end{tabular}
	\vspace{-5pt}
\end{table}

We also study the sensitivity of our algorithm with respect to the number of tuples used for structure learning. Table~\ref{tbl_tuples} shows the compression ratio when we use $1000$, $2000$ and $5000$ tuples in the structure learning algorithm respectively. As we can see, the compression ratio improves gradually as we use more tuples for structure learning.

\begin{table}[h]
	\vspace{-15pt}
	\caption{Sensitivity to Number of Tuples}\label{tbl_tuples}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		Number of Tuples & 1000 & 2000 & 5000 \\
		\hline
		Comp. Ratio & 0.0474 & 0.0460 & 0.0427 \\
		\hline
	\end{tabular}
	\vspace{-15pt}
\end{table}


%Figure~\ref{fig_detected_correlation} shows the effect of incomplete Bayesian Network structure on the performance of our compression algorithm. The x axis shows the number of tuples used for learning the Bayesian Network. We define $\mathcal{B}$ to be the Bayesian Network that we learned from the subset of tuples, and $\mathcal{B}_{\emptyset}$ to be the empty Bayesian Network (i.e., all random variables are independent). We measure the amount of correlation detected in $\mathcal{B}$ as follows:
%$$ \text{Detected Correlation} = 1 - \frac{\text{size of compressed data using } \mathcal{B}}{\text{size of compressed data using } \mathcal{B}_{\emptyset}} $$

%\begin{figure}[h]
%	\centering
%	\vspace{-5pt}
%	\includegraphics[width = 5cm, height = 3cm]{detected_correlation.png}
%	\vspace{-5pt}
%	\caption{BN Training Data vs. Detected Correlation}\label{fig_detected_correlation}
%\end{figure}

%\begin{figure}[h]
%	\vspace{-15pt}	
%	\centering
%	\includegraphics[width = 5cm, height = 3cm]{bn_edges.png}
%	\vspace{-5pt}	
%	\caption{BN Training Data vs. BN Edges}\label{fig_detected_edges}
%\end{figure}

%The detected correlation measure reflects the additional percentage of compression that we can achieve by using $\mathcal{B}$ instead of $\mathcal{B}_{\emptyset}$. Intuitively, it is the percentage of compression resulted by exploiting correlations between attributes. As we can see, the amount of detected correlation increases steadily as we use more training data for learning a Bayesian Network, but the gain diminishes as the training data size increases (note that the x axis is in log scale).

%Figure~\ref{fig_detected_edges} shows the number of edges in the Bayesian Network $\mathcal{B}$. Clearly, as we use more training data, the number of edges (i.e., the number of detected dependencies in the dataset) in $\mathcal{B}$ also increases. However, as the number of detected dependencies increase, it becomes harder to detect new dependencies.
