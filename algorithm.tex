%!TEX root=DBCompress.tex


\section{Structure Learning}\label{sec_algorithm}

\begin{figure}[h]
    \centering
    \includegraphics[width = 6cm]{flowchart.png}
    \vspace{-10pt}
    \caption{Workflow of the Compression and Decompression Algorithm}\label{fig_workflow_overview}
    \vspace{-15pt}
\end{figure}

The overall workflow of \system\ is illustrated in Figure~\ref{fig_workflow_overview}. 
\system \\ uses a combination of Bayesian networks and arithmetic coding for compression.
The workflow of the compression algorithm is the following:

\begin{enumerate}
	\item
	Learn a Bayesian network structure from the dataset, which captures the dependencies between attributes in the structure graph, and models the conditional probability distribution of each attribute conditioned on all the parent attributes.
	\item
	Apply arithmetic coding to compress the dataset, using the Bayesian network as probabilistic models.
	\item
	Concatenate the model description file (describing the Bayesian network model) and compressed dataset file.
\end{enumerate}
In this section, we focus on the first step of this workflow.
We focus on the remaining steps (along with decompression) in Section~\ref{sec_implementation}.


% Some example datasets are provided in Section~\ref{sec_example_dataset} to demonstrate the behavior of the compression algorithm.

Although the problem of Bayesian network learning has been extensively studied in literature~\cite{koller2009probabilistic}, conventional objectives like Bayesian Information Criterion (BIC)~\cite{schwarz1978estimating} are suboptimal for the purpose of compressing datasets. In Section~\ref{sec_model_learning}, we derive the correct objective function for learning a Bayesian network that minimizes the size of the compressed dataset and explain how to modify existing Bayesian network learning algorithms to optimize this objective function.

The general idea about how to apply arithmetic coding on a Bayesian network is straightforward: since the graph encoding the structure of a Bayesian Network is acyclic, we can use any topological order of attributes and treat the attribute values as the sequence of symbols in arithmetic coding. However, arithmetic coding does not naturally apply to non-categorical attributes.
In Section~\ref{sec_probtree}, we introduce \probtree, the mechanism for supporting non-categorical and arbitrary user-defined attribute types in \system. \probtree~is the interface for every attribute type in \system, and example \probtree s for categorical, numerical and string attributes are demonstrated in Section~\ref{sec_example_model} to illustrate the wide applicability of this interface. We describe the \probtree \ API in Section~\ref{sec_api}.

%Compare to other general purpose probabilistic models (e.g., Markov Network), Bayesian Networks have the advantage of implicitly assuming an order among random variables, which makes them especially suitable as probabilistic models for Arithmetic Coding~\cite{davies1999bayesian}. 

% As stated in Section~\ref{sec_bayesian}, a Bayesian Network contains a directed acyclic structure graph and a collection of conditional distribution for each node. We will discuss how to choose parent sets and define conditional probability distributions in Section~\ref{sec_model_learning}.

\subsection{Learning a Bayesian Network: The Basics}\label{sec_model_learning}

Many Bayesian network learning algorithms search for the optimal Bayesian network by minimizing some objective function (e.g., negative log-likelihood, BIC~\cite{schwarz1978estimating}). These algorithms usually have two separate components~\cite{koller2009probabilistic}:
\begin{denselist}
	\item
	A combinatorial optimization component that searches for a graph with optimal structure.
	\item
	A score evaluation component that evaluates the objective function given a graph structure.
\end{denselist}

The two components above are independent in many algorithms. In that case, we can modify an existing Bayesian network learning algorithm by changing the score evaluation component, while still using the same combinatorial optimization component. In other words, for any objective function, as long as we can efficiently evaluate it based on a fixed graph structure, we can modify existing Bayesian network learning algorithms to optimize it. 

In this section, we derive a new objective function for learning a Bayesian network that minimizes the size of compressed dataset. We show that the new objective function can be evaluated efficiently given the structure graph. Therefore existing Bayesian Network learning algorithms can be used to optimize it.

% When learning a Bayesian Network, the goal is to minimize the total size of the compressed file. This objective is known as minimum description length principle in statistical modeling literature~\cite{barron1998minimum}. 

Suppose our dataset $D$ consists of $n$ tuples, and each tuple $t_i$ contains $m$ attributes $a_{i1}, a_{i2}, \ldots, a_{im}$. Let $\mathcal{B}$ be a Bayesian network that describes a joint probability distribution over the attributes. Clearly, $\mathcal{B}$ contains $m$ nodes, each corresponding to an attribute.

The total description length of $D$ using $\mathcal{B}$ is $\textbf{S}(D|\mathcal{B}) = \textbf{S}(\mathcal{B}) + \textbf{S}(\textit{Tuples}|\mathcal{B})$,
where $\textbf{S}(\mathcal{B})$ is the size of description file of $\mathcal{B}$, and $\textbf{S}(\textit{Tuples}|\mathcal{B})$ is the total length of encoded binary strings of tuples using arithmetic coding. For the model description length $\textbf{S}(\mathcal{B})$, we have $\textbf{S}(\mathcal{B}) = \sum_{i = 1}^m \textbf{S}(\mathcal{M}_i)$, 
where $m$ is the number of attributes in our dataset, and $\mathcal{M}_1, \ldots, \mathcal{M}_m$ are the models for each attribute in $\mathcal{B}$. The expression
$\textbf{S}(\textit{Tuples}|\mathcal{B})$ is just the sum of the $\textbf{S}(t_i|\mathcal{B})$s (the lengthes of the encoded binary string for each $t_i$). %$\textbf{S}(\textit{Tuples}|\mathcal{B}) = \sum_{t_i \in \textit{DB}} \textbf{S}(t_i|\mathcal{B}) $

We have the following decomposition of $\textbf{S}(t_i|\mathcal{B})$:
\begin{align*}
\textbf{S}(t_i|\mathcal{B}) \approx & - \sum_{j=1}^m \log_2 \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j) \\
									& - \sum_{j=1}^m \textit{num}(a_{ij}) \log_2 \epsilon_j + \text{const}
\end{align*}
where $\textit{parent}(a_{ij})$ is the set of parent attributes of $a_{ij}$ in $\mathcal{B}$, $\textit{num}(a)$ is the indicator function of whether $a$ is a numerical attribute or not, and $\epsilon_j$ is the maximum tolerable error for attribute $a_{ij}$. We will justify this decomposition in Section~\ref{sec_example_model}, after we introduce the encoding scheme for numerical attributes.

%A nice property of Bayesian Network is that the log-likelihood can be decomposed as follows~\cite{koller2009probabilistic}:
%$$ \sum_{t_i \in \textit{DB}} \log \mathbf{Pr}(t_i|\mathcal{B}) = \sum_{t_i \in \textit{DB}} \sum_{j=1}^m \log \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j) $$

Therefore, the total description length $\textbf{S}(D|\mathcal{B})$ can be decomposed as follows:
\begin{align*}
\textbf{S}(D|\mathcal{B}) \approx & \sum_{j=1}^m [\textbf{S}(\mathcal{M}_j) - \sum_{t_i \in \textit{DB}} \log_2 \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j)] \\
				& - n (\sum_{j=1}^m \textit{num}(a_j) \log_2 \epsilon_j + \text{const})
\end{align*}

Note that the term in the second line does not depend on either $\mathcal{B}$ or $\mathcal{M}_i$. Therefore we only need to optimize the first summation. We denote each term in the first summation on the right hand side as $obj_j$:
$$ obj_j = \textbf{S}(\mathcal{M}_j) - \sum_{t_i \in \textit{DB}} \log \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j) $$

For each $obj_j$, if the network structure (i.e., $\textit{parent}(a_{ij})$) is fixed, then $obj_j$ only depends on $\mathcal{M}_j$. In that case, optimizing $\textbf{S}(D|\mathcal{B})$ is equivalent to optimizing each $obj_j$ individually. In other words, if we fix the Bayesian network structure in advance, then the parameters of each model can be learned separately. 

Optimizing $obj_j$ on $\mathcal{M}_j$ is exactly the same as maximizing likelihood. For many models, a closed-form solution for identifying maximum likelihood parameters exists. In such cases, the optimal $\mathcal{M}_j$ can be quickly determined and the objective function $\textbf{S}(D|\mathcal{B})$ can be computed efficiently.

\vspace{2mm}

\noindent \textbf{Structure Learning}

\vspace{1mm}

\noindent In general, searching for the optimal Bayesian network structure is NP-hard~\cite{koller2009probabilistic}. In \system, we implemented a simple greedy algorithm for this task. The algorithm starts with an empty seed set, and repeatedly finds new attributes with the lowest $obj_j$, and adds these new attributes to the seed set. \techreport{The pseudo-code is shown in Algorithm~\ref{alg_structure_learning}.}\paper{The pseudo-code can be found in our technical report~\cite{techreport}.}

\techreport{
\begin{algorithm}
	%\scriptsize
	\caption{Greedy Structure Learning Algorithm}
	\label{alg_structure_learning}
	\begin{algorithmic}
		\Function{LearnStructure}{}
		\State $\text{seed} \leftarrow \emptyset$
		\For{$i = 1$ to $m$}
			\For{$j = 1$ to $m$}				
				\State $\text{parent} \leftarrow \emptyset$
				\While{true}
					\State $\text{best\_model\_score} \leftarrow obj(\text{parent} \rightarrow j)$
					\State $\text{best\_model} \leftarrow \text{parent}$
					\For{$k \in \text{seed}$}
						\State $t \leftarrow obj(\text{parent} \cup \{k\} \rightarrow j)$
						\If{$t < \text{best\_model\_score}$}
							\State $\text{best\_model\_score} \leftarrow t$
							\State $\text{best\_model} \leftarrow \text{parent} \cup \{k\}$
						\EndIf
					\EndFor
					\If{$\text{best\_model} = \text{parent}$}
						\State \textbf{break}
					\Else
						\State $\text{parent} \leftarrow \text{best\_model}$
					\EndIf
				\EndWhile
				\State $\text{score}_j = obj(\text{parent} \rightarrow j)$
			\EndFor
			\State Find $j$ with minimum $\text{score}_j$
			\State $\text{seed} \leftarrow \text{seed} \cup \{j\}$
		\EndFor
		\EndFunction
	\end{algorithmic}
\end{algorithm}
}

\paper{The greedy algorithm} \techreport{Algorithm~\ref{alg_structure_learning}} has a worst case time complexity of $O(m^4 n)$ where $m$ is the number of columns and $n$ is the number of tuples in the dataset. For large datasets, even this simple greedy algorithm is not fast enough. However, note that the objective values \techreport{$obj(\text{parent} \rightarrow j)$}\paper{$obj_j$} are only used to compare different models. So we do not require exact values for them, and some rough estimation would be sufficient. Therefore, we can use only a subset of data for the structure learning to improve efficiency. %(The impact of this change is evaluated in the experiments.)

\subsection{Supporting Complex Attributes}\label{sec_probtree}

\vspace{2mm}

\noindent \textbf{Encoding and Decoding Complex Attributes}

\vspace{1mm}

\noindent Before applying arithmetic coding on a Bayesian network to compress the dataset as we stated earlier, there are two issues that we need to address first:
\begin{itemize}
	\item
	Arithmetic Coding requires a finite alphabet for each symbol. However, it is natural for attributes in a dataset to have infinite range (e.g., numerical attributes, strings). 
	\item
	In order to support user-defined data types, we need to allow the user to specify a probability distribution over an unknown data type.
\end{itemize}

To address these difficulties, we introduce the concept of {\probtree}, short for \system~Interface for Data types. A \probtree~is a (possibly infinite) decision tree~\cite{witten2005data} with non-negative probabilities associated with edges in the tree, such that for every node $v$, the probabilities of all the edges connecting $v$ and $v$'s children sum to one. 

\begin{figure}[h]
	\centering
	\vspace{-10pt}
	\includegraphics[width = 6cm]{prob_tree_example.png}
	\vspace{-10pt}
	\caption{\probtree~Example}\label{fig_prob_tree_example}
	\vspace{-10pt}
\end{figure}

Figure~\ref{fig_prob_tree_example} shows an example infinite \probtree~for a positive numerical attribute $X$. As we can see, each edge is associated with a decision rule and a non-negative probability. For each non-leaf node $v_{2k-1}$, the decision rules on the edges between $v_{2k-1}$ and its children $v_{2k}$ and $v_{2k+1}$ are $x \leq k$ and $x > k$ respectively. Note that these two decision rules do not overlap with each other and covers all the possibilities. The probabilities associated with these two rules sum to $1$, which is required in \probtree. This \probtree~describes the following probability distribution over $X$:
$$ \mathbf{Pr}(X \in (k - 1, k]) = 0.9^{k-1} \times 0.1 $$

In Section~\ref{sec_implementation}, we will show that we can encode or decode an attribute using Arithmetic Coding if the probability distribution of this attribute can be represented by a \probtree.

As shown in Figure~\ref{fig_prob_tree_example}, a \probtree~naturally controls the maximum tolerable error in a lossy compression setting. Each leaf node $v$ corresponds to a subset $A_v$ of attribute values such that for every $a \in A_v$, if we start from the root and traverse down according to the decision rules, we will eventually reach $v$. As an example, in Figure~\ref{fig_prob_tree_example}, for each leaf node $v_{2k}$ we have $A_{v_{2k}} = (k-1,k]$. Let $a_v$ be the representative attribute value of a leaf node $v$, then the maximum possible recovery error for $v$ is:
$$\epsilon_v = \sup_{a \in A_v} \text{distance}(a, a_v)$$

Let $T_i$ be the \probtree~corresponding to the $i$th attribute. As long as for every $v \in T_i$, $\epsilon_v$ is less than or equal to the maximum tolerable error $\epsilon_i$, we can satisfy the closeness constraint (defined in Section~\ref{sec_prob_def}).

\vspace{2mm}

\noindent \textbf{Using User-defined Attributes as Predictors}

\vspace{2mm}

To allow user-defined attributes to be used as predictors for other attributes, we introduce the concept of attribute interpreters, which translate attributes into either categorical or numerical values. In this way, these attributes can be used as predictors for other attributes. 

The attribute interpreters can also be used to capture the essential features of an attribute. For example, a numerical attribute could have a categorical interpreter that better captures the internal meaning of the attribute. This process is similar to the feature extraction procedure in many data mining applications, and may improve compression rate.

\subsection{Example {\large \probtree s}}\label{sec_example_model}

In \system, we have implemented models for three primitive data types. We intended these models to both illustrate how \probtree s can be used to define probability distributions, and also to cover most of the basic data types, so the system can be used directly by casual users without writing any code. 

%The following three types of models are implemented in our system:
We implemented models for the following types of attributes:
\begin{denselist}
	\item
	Categorical attributes with finite range.
%	Table-based categorical model, which encodes categorical attribtues and requires every parent attribute to have categorical interpreter.
	\item
	Numerical attributes, either integer or float number.
%	Table-based numerical model, which encodes numerical attributes and requires every parent attribute to have categorical interpreter.
	\item
	String attributes
%	String model, which encodes string attributes, with no parent attributes allowed.
\end{denselist}

\techreport{In general, an attribute can be represented by \probtree~if it can be constructed using several simple steps. For any given probability distribution, there might be more than one possible \probtree~design. As long as the underlying distribution is correctly encoded, the compression rate will be the same. However, the size of the \probtree could vary and will directly affect the efficiency of the compression algorithm, so \probtree should be carefully designed if high efficiency is desired.}

\vspace{2mm}

\noindent \textbf{Categorical Attributes}

\vspace{2mm}

The distribution over a categorical attributes can be represented using a trivial one-depth \probtree. 

\vspace{2mm}

\noindent \textbf{Numerical Attributes}

\vspace{2mm}

For a numerical attribute, we construct the \probtree~using the idea of {\em bisection}. Each node $v$ is marked with an upper bound $v_r$ and a lower bound $v_l$, so that every attribute value in range $(v_l,v_r]$ will pass by $v$ on its path from the root to the corresponding leaf node. Each node has two children and a bisecting point $v_m$, such that the two children have ranges $(v_l, v_m]$ and $(v_m, v_r]$ respectively. The branching process stops when the range of the interval is less than $2 \epsilon$, where $\epsilon$ is the maximum tolerable error.
Figure~\ref{fig_numerical_prob_tree} shows an example \probtree~for numerical attributes.

\begin{figure}[h]
	\centering
	\includegraphics[width = 7cm]{numerical_prob_tree.png}
	\vspace{-15pt}
	\caption{\probtree~for numerical attributes}\label{fig_numerical_prob_tree}
	\vspace{-10pt}
\end{figure}

Since each node represents a continuous interval, we can compute its probability using the cumulative distribution function. The branching probability of each node is:
\begin{align*}
 & (\mathbf{Pr}(\textit{left branch}), \mathbf{Pr}(\textit{right branch})) \\
= & (\frac{\mathbf{Pr}(v_l < X \leq v_m)}{\mathbf{Pr}(v_l < X \leq v_r)}, \frac{\mathbf{Pr}(v_m < X \leq v_r)}{\mathbf{Pr}(v_l < X \leq v_r)})
\end{align*}

%As for computing the objective value, the likelihood part can be approximated as:
%$$ \mathcal{L}(a_i|\mu, b) \approx \mathbf{Pr}() $$

Clearly, the average number of bits that is needed to encode a numerical attribute depends on both the probability distribution of the attribute and the maximum tolerable error $\epsilon$. The following theorem gives us a lower bound on the average number of bits that is necessary for encoding a numerical attribute (the proof can be found in \techreport{the appendix}\paper{our technical report~\cite{techreport}}):
%It also shows that the average number of bits used by the above bisecting encoding scheme is very close to this lower bound. 

\begin{theorem}\label{thm_numerical}
	Let $X \in \mathcal{X} \subseteq \mathbb{R}$ be a numerical random variable with continuous support $\mathcal{X}$ and probability density function $f(X)$. Let $g: \mathcal{X} \rightarrow \{0,1\}^*$ be any uniquely decodable encoding function, and $h: \{0,1\}^* \rightarrow \mathcal{X}$ be any decoding function. If there exists a function $\rho:\mathcal{X} \rightarrow \mathbb{R}^+$ such that:
	\begin{equation}\label{eqn_self_bounded}
	\forall x, y \in \mathcal{X}, |x - y| < 2\epsilon \Rightarrow |f(x) - f(y)| \leq \rho(x) f(x) |x - y|
	\end{equation}
	and $g, h$ satisfies the $\epsilon$-closeness constraint:
	$$ \forall x \in \mathcal{X}, |h(g(x)) - x| \leq \epsilon $$
	Then
	\begin{align*}
	E_X[len(g(X))] \geq & E_X[- \log_2 f(X)] - \\
						& E_X[\log_2(2 \epsilon \rho(X) + 1)] - \log_2 \epsilon - 2
	\end{align*}
	Furthermore, if $g$ is the bisecting code described above, then
	\begin{align*}
	E_X[len(g(X))] \leq & E_X[- \log_2 f(X)] - \log_2 l + \\
						& E_X[\max(\log_2 \rho(X) + \log_2 l, 0)] + 4
	\end{align*}

	where $l = \min_{v} (v_r - v_l)$ is the minimum length of probability intervals in the tree.
\end{theorem}

Equation~(\ref{eqn_self_bounded}) is a mild assumption that holds for many common probability distributions, including uniform distribution, Gaussian distribution, and Laplace distribution~\cite{abramowitz1964handbook}. 

To understand the intuition behind the results in Theorem~\ref{thm_numerical}. Let us consider a Gaussian distribution as an example:
$$ f(x|\mu,\sigma) = \frac{1}{\sigma \sqrt{2 \pi}} e^{-\frac{(x - \mu)^2}{2\sigma^2}} $$
In this case,
$$ \rho(x) = \frac{|x - \mu| + 2\epsilon}{\sigma^2} $$
Substituting into the first expression, we have:
\begin{align*}
E_X[len(g(X))] \geq & \log_2 \sigma - \log_2 \epsilon + \log_2 \sqrt{2 \pi} - \frac{3}{2} - \\
					& E_X[\log_2 (\frac{2 \epsilon(|X - \mu| + 2\epsilon)}{\sigma^2} + 1)]
\end{align*}
Note that when $\epsilon$ is small compared to $\sigma$ (e.g., $\epsilon < \frac{1}{10} \sigma$), the last term is approximately zero. Therefore, the number of bits needed to compress $X$ is approximately $\log_2 \frac{\sigma}{\epsilon}$.

Now consider the second result,
\begin{align*}
E_X[len(g(X))] \leq & \log_2 \sigma - \log_2 \frac{l}{2} + \log_2 \sqrt{2 \pi} + \frac{7}{2} + \\
& E_X[\max(\log_2 \frac{l(|X - \mu| + 2\epsilon)}{\sigma^2}, 0)]
\end{align*}
Let $l = 2 \epsilon$, and when $\epsilon < \frac{1}{10} \sigma$, the last term is approximately zero. Comparing the two results, we can see that the bisecting scheme achieves near optimal compression rate.
%For most practical settings, $\epsilon$ is less than the standard deviation of the distribution. In these cases, the term related to $\rho(x)$ usually can be bounded by a small constant. Hence, Theorem~\ref{thm_numerical} has two implications: for small $\epsilon$, it contributes to the average code word length as an additive factor $- \log_2 \epsilon$,  and the bisecting scheme achieves near-optimal compression rate for small $\epsilon$.

Theorem~\ref{thm_numerical} can be used to justify the decomposition in Section~\ref{sec_model_learning}. Recall that we used the following expression as an approximation of $len(g(X))$:
$$ len(g(X)) \approx - \log_2 f(X) - \log_2 \epsilon + \text{const} $$
Compared to either the upper bound or lower bound in Theorem~\ref{thm_numerical}, the only term we omitted is the term related to $\rho(X)$. As we have seen in the Gaussian case, this term is approximately zero when $\epsilon$ is smaller than $\frac{\sigma}{10}$ where $\sigma$ is the standard deviation parameter. The same conclusion is also true for the Laplace distribution~\cite{abramowitz1964handbook} and the uniform distribution.

\vspace{2mm}
\noindent \textbf{String Attributes}
\vspace{2mm}

The \probtree~for string attributes can be viewed as having two steps:
\begin{enumerate}
	\item
	determine the length of the string
	\item
	determine the characters of the string
\end{enumerate}
The length of a string can be viewed as an integer, so we can use exactly the same bisecting rules as for numerical attributes. After that, we use $n$ more steps to determine each character of the string, where $n$ is the string's length. The probability distribution of each character can be specified by conventional probabilistic models like the $k$-gram model.

\begin{table*}[t]
	\caption{Functions need to be implemented for \probtree~Template}\label{tbl_probtree}
	\begin{center}
		\begin{tabular}{| c | c |}
			\hline
			\textbf{Function} & \textbf{Description} \\
			\hline
			IsEnd & Return whether the current node is a leaf node. \\
			\hline
			GenerateBranch & Return the number of branches and the probability distribution associated with them. \\
			\hline
			GetBranch & Given an attribute value, return which branch does the value belong to. \\
			\hline
			ChooseBranch & Set the current node to another node at next level according to the given branch index. \\
			\hline
			GetResult & If the current node is a leaf node, return the representative attribute value of this node. \\
			\hline
		\end{tabular}
	\end{center}
\paper{	\vspace{-20pt} }
\end{table*}

\techreport{
\begin{table*}[t]
	\vspace{-25pt}
	\caption{Functions for \probmodel~Abstract Class}\label{tbl_probmodel}
	\begin{center}
		\begin{tabular}{| c | c |}
			\hline
			\textbf{Function} & \textbf{Description} \\
			\hline
			GetProbTree & Given the values of parent attributes, return a \probtree~instance. \\
			\hline
			ReadTuple & Read a tuple into the model. \\
			\hline
			EndOfData & Indicate the end of dataset. All gathered statistics can be used to find the optimal parameters. \\
			\hline
			GetModelCost & Return an estimate of the objective value $\textit{obj}_j = \textbf{S}(\mathcal{M}_j) - \sum_{t_i \in \textit{DB}} \log \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j)$. \\
			\hline
			WriteModel & Write the description of the model to a file. This function will be called only after EndOfData is called.\\
			\hline
			ReadModel & Read the description of the model from a file. This function will be called to initialize the \probmodel.\\
			\hline
		\end{tabular}
	\end{center}
	\vspace{-15pt}
\end{table*}
}
\subsection{{\large \probtree}~API}\label{sec_api}

In \system, \probtree~is defined as an abstract class~\cite{cppreference}. There are five functions that are required to be implemented in order to define a new data type using \probtree. These five functions allow the system to interactively explore the \probtree~class: initially, the current node pointer is set to the root of \probtree; each function will either acquire information about the current node, or move the current node pointer to one of its children. Table~\ref{tbl_probtree} lists the five functions together with their high level description.
%\agp{Explain these functions, even if it is at a high level.}

We also develop another abstract class called \probmodel. A \probmodel~first reads in all the tuples in the dataset, then generates a \probtree~instance and an estimation of the objective value $\textit{obj}_j$ derived in Section~\ref{sec_model_learning}:
$$ \textit{obj}_j = \textbf{S}(\mathcal{M}_j) - \sum_{t_i \in \textit{DB}} \log \mathbf{Pr}(a_{ij}|\textit{parent}(a_{ij}), \mathcal{M}_j) $$

There are two reasons behind this design:
\begin{itemize}
	\item
	For a parametric \probtree, the parameters need to be learned using the dataset at hand.
	\item
	The Bayesian network learning algorithm requires an estimation of the objective value. Although it is possible to compute the objective value by actually compressing the attributes, in many cases it is much more efficient to approximate it directly.
\end{itemize}

\probmodel~requires six functions to be implemented. These functions allow the \probmodel~to iterate over the dataset and generate \probtree~instances. \paper{The specification of these functions and the psuedo-code of their interactions with \probtree~can be found in our technical report~\cite{techreport}.}\techreport{Table~\ref{tbl_probmodel} listed the six functions together with their high level explanation.}

A \probmodel~instance is initialized with the target attribute and the set of predictor attributes. After that, the model instance will read over all tuples in the dataset and need to decide the optimal choice of parameters. The model also needs to return an estimate of the objective value $obj_j$, which will be used in the Bayesian network structure learning algorithm \techreport{(i.e., Algorithm~\ref{alg_structure_learning})} to compare models. Finally, \probmodel~should be able to generate \probtree~instances based on parent attribute values.

\techreport{
\vspace{2mm}

\noindent \textbf{Usage of Abstract Classes in the System}

\vspace{2mm}

Algorithm~\ref{alg_api_calls} shows how our system interacts with these abstract classes. In the model learning phase, the \probmodel~is used to estimate the objective value $obj_j$. In the compression and decompression phase, \probmodel~is used to generate \probtree~instances, which are then used to generate probability intervals for tuples.

\begin{algorithm}
	%\scriptsize
	\caption{API Calls to Abstract Classes}
	\label{alg_api_calls}
	\begin{algorithmic}
		\Function{ComputeObj}{$\text{parent} \rightarrow j$}
		\State $\text{model} \leftarrow \textbf{new } \text{\probmodel}(\text{parent} \rightarrow j)$
		\For{$i = 1$ to $n$}
		\State $\text{model}.ReadTuple(t_i)$
		\EndFor
		\State $\text{model}.EndOfData()$
		\State \Return $\text{model}.GetModelCost()$
		\EndFunction
		
		\Function{Compression}{$t_i, \{M_1, \ldots, M_m\}$}
		\State $\text{PIseq} \leftarrow \emptyset$
		\For{$j = 1$ to $m$}
		\State $T_j \leftarrow M_j.GetProbTree(t_i, \text{parent}_j)$
		\While{\textbf{not} $T_j.IsEnd()$}
		\State $branches \leftarrow T_j.GenerateBranch()$
		\State $b \leftarrow T_j.GetBranch(a_{ij})$
		\State $[l,r] \leftarrow \text{ComputeProbabilityInterval}(branches, b)$
		\State $\text{PIseq} \leftarrow \text{PIseq} + \{[l,r]\}$
		\State $T_j.ChooseBranch(b)$
		\EndWhile
		\State $a_{ij} \leftarrow T_j.GetResult()$
		\EndFor
		\State \Return $\text{ArithmeticCoding}(\text{PIseq})$
		\EndFunction
		
		\Function{Decompression}{$\{M_1, \ldots, M_m\}$}
		\State $t_i \leftarrow \emptyset$
		\State $d \leftarrow \textbf{new} \text{ Decoder}()$
		\For{$j = 1$ to $m$}
		\State $T_j \leftarrow M_j.GetProbTree(t_i, \text{parent}_j)$
		\While{\textbf{not} $T_j.IsEnd()$}
		\State $branches \leftarrow T_j.GenerateBranch()$
		\State $b \leftarrow d.GetNextBranch(branches)$
		\State $T_j.ChooseNextBranch(b)$
		\EndWhile
		\State $a_{ij} \leftarrow T_j.GetResult()$
		\EndFor
		\State \Return $t_i$
		\EndFunction
	\end{algorithmic}
\end{algorithm}
}

%\subsection{Parameter Fitting}\label{sec_param_fitting}

%The parameter fitting algorithm is used as a subroutine in the Bayesian Network structure learning algorithm. For any attribute $i$ and a set of parent attributes $p_{i1}, \ldots, p_{ik}$, we need to find the optimal model $\mathcal{M}_i$ such that
%$$ Q(\mathcal{M}_i) = \textbf{S}(\mathcal{M}_i) - \sum_{t \in \textit{DB}} \log \mathbf{Pr}(t_i|t_{p_{i1}}, t_{p_{i2}}, \ldots, t_{p_{ik}}, \mathcal{M}_i)$$
%is minimized. The value of $Q(\mathcal{M}_i)$ is also of importance, so we need to be able to compute it, or at least roughly estimate it.

%While it is completely possible to implement more types of models to support various data types and achieve higher compression rate. Our choice of the three models are particularly easy to implement and the optimal parameters have a nice closed form.

%For each of the three above models, the storage cost $\textbf{S}(\mathcal{M}_i)$ is fixed and independent of the particular parameter choice:
%\begin{itemize}
%	\item
%	For table-based categorical model, suppose that the parent attribute $p_{ij}$ takes value from set $\textit{Val}_{p_{ij}}$ and attribute $i$ takes value from set $\textit{Val}_i$. We need to store parameters for $\prod_{j=1}^k |\textit{Val}_{p_{ij}}|$ conditional distributions, each has $|\textit{Val}_i| - 1|$ float number parameters. The total storage cost is:
%	$$ \textbf{S}(M_i) = \prod_{j=1}^k |Val_{p_{ij}}| (|Val_i| - 1) \times \textbf{size}(\textit{float}) $$
%	\item
%	For table-based numerical model, we also need to store parameters for $\prod_{j=1}^k |\textit{Val}_{p_{ij}}|$ conditional distributions. We use Laplace distribution as the conditional distribution, which has two parameters $\mu$ and $b$:
%	$$ \mathbf{Pr}(x|\mu, b) = \frac{1}{2b} \exp(- \frac{|x - \mu|}{b}) $$
%	The total storage cost is:
%	$$ \textbf{S}(M_i) = 2 \prod_{j=1}^k |Val_{p_{ij}}| \times \textbf{size}(\textit{float}) $$
%	\item
%	For string model, we store a distribution among possible characters. For standard ASCII string, we need to store $255$ float numbers:
%	$$ \textbf{S}(M_i) = 255 \times \textbf{size}(\textit{float}) $$
%\end{itemize}

%Note that in the above models, the choice of parameters does not affect $\textbf{S}(M_i)$, therefore minimizing $Q(\mathcal{M}_i)$ is equivalent as maximizing likelihood:
%$$ \mathcal{M}_i = \arg \max_\mathcal{M} \sum_{t \in \textit{DB}} \log \mathbf{Pr}(t_i|t_{p_{i1}}, t_{p_{i2}}, \ldots, t_{p_{ik}}, \mathcal{M}) $$
%This property holds for many models with fixed number of parameters. However for some models the number of parameters is variable (Decision Tree for example). In those cases the optimization process could be more complicated.

%However, as the target attribute is of continuous natural, we need to take into account the precision we want to achieve. Suppose we can tolerate $\epsilon$ deviation from its true value, then the number of bits that we need to describe one attribute $x$ is about:
%$$ \textit{Len}(x|\mu, b) = - \log (2 \epsilon \mathbf{Pr}(x|\mu, b)) = - \log (\frac{\epsilon}{b} \exp( - \frac{|x - \mu|}{b})) $$
%Therefore, the total model description length is:
%$$ Q(M_i) = \prod_{j=1}^k |Val_{p_{ij}}| \times 2 \textbf{size}(\textit{float}) + \sum_{t \in \textit{DB}} \log (2 \epsilon \mathbf{Pr}(t_i | \ldots)) $$
%\item
%For string model, we can describe how likely each character will appear in the string. For standard ascii string, this requires $255 \textbf{size}(\textit{float})$ bits to describe. Then the string will be assumed to follow character-wise unigram model. 

%Finding maximum likelihood estimator of parameters for table-based categorical models and string models are trivial and can be found in most statistics/machine learning textbooks~\cite{koller2009probabilistic}. For table-based numerical models, the maximum likelihood estimator of $\mu$ is the median of data, which can only be approximately estimated with limited avaiable memory space. Greenwald and Khanna~\cite{greenwald2001space} developed algorithm that approximates median of data using only logarithmic space, the algorithm can be modified  and used here to simultaneously estimate $\mu$ and $b$ within one pass of data.

%The parameter fitting process must be done with limited memory space (It is because we need to train multiple models simultaneously, as we will see in Section~\ref{sec_struct_learning}), and preferably if it can be done within simple pass of the data. Such streaming data processing situation add some additional complexity to parameter fitting process. In particular for table-based numerical model, we are unable to find the maximum likelihood estimation within single pass of data, and approximation algorithm is used to make sure that some reasonably good estimations can be found within single data pass.

%Finally we need to compute $Q(\mathcal{M}_i)$, which is straightforward for categorical attributes. For numerical attributes we need to be more careful because $\mathbf{Pr}(t_i|t_{p_{i1}}, \ldots, t_{p_{ik}})$ usually represents density of probability. To convert to probability measure we need to multiply it by the ``bin size'' $2 \epsilon_i$:
%$$ \mathbf{Pr}(t_i \in [e_i - \epsilon_i, e_i + \epsilon_i]|t_{p_{i1}}, \ldots, t_{p_{ik}}) \approx 2 \epsilon_i \mathbf{Pr}(t_i|t_{p_{i1}}, \ldots, t_{p_{ik}}) $$
%where $e_i$ is the center of the bin that $t_i$ falls into.

%\todo{Add a figure for the above paragraph}


