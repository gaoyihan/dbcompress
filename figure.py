import math
import matplotlib.pyplot as pyplot

def plot_compression_rate_forest_cover():
    plot_error = [0.5,1,5,10]
    plot_itcompress = [0.3, 0.27, 0.18, 0.13]
    plot_gzip = [0.145, 0.145, 0.145, 0.145]
    plot_spartan_gzip = [0.095, 0.09, 0.075, 0.055]
    plot_itcompress_gzip = [0.1, 0.095, 0.06, 0.03]
    plot_bn = [0.058, 0.0517, 0.0387, 0.033]
    
    pyplot.figure(figsize=(6,4))
    pyplot.plot(plot_error, plot_itcompress, '-<')
    pyplot.plot(plot_error, plot_gzip, '--o')
    pyplot.plot(plot_error, plot_spartan_gzip, '-.v')
    pyplot.plot(plot_error, plot_itcompress_gzip, ':^')
    pyplot.plot(plot_error, plot_bn, '->')
    pyplot.legend(['ItCompress', 'Gzip', 'Spartan(gzip)', 'ItCompress(gzip)', 'Squish'], prop = {'size':11} )
    pyplot.xlabel('Error Tolerance(%)', fontsize='large')
    pyplot.ylabel('Compression Ratio', fontsize='large')
    pyplot.tight_layout(pad=0)
    pyplot.show()

def plot_compression_rate_corel():
    plot_error = [0.5,1,5,10]
    plot_itcompress = [0.46, 0.42, 0.25, 0.17]
    plot_gzip = [0.38, 0.38, 0.38, 0.38]
    plot_spartan_gzip = [0.29, 0.28, 0.18, 0.115]
    plot_itcompress_gzip = [0.2, 0.15, 0.08, 0.02]
    plot_bn = [0.049, 0.04, 0.024, 0.019]
    
    pyplot.figure(figsize=(6,4))
    pyplot.plot(plot_error, plot_itcompress, '-<')
    pyplot.plot(plot_error, plot_gzip, '--o')
    pyplot.plot(plot_error, plot_spartan_gzip, '-.v')
    pyplot.plot(plot_error, plot_itcompress_gzip, ':^')
    pyplot.plot(plot_error, plot_bn, '->')
    pyplot.legend(['ItCompress', 'Gzip', 'Spartan(gzip)', 'ItCompress(gzip)', 'Squish'], prop = {'size':11} )
    pyplot.xlabel('Error Tolerance(%)', fontsize='large')
    pyplot.ylabel('Compression Ratio', fontsize='large')
    pyplot.tight_layout(pad=0)
    pyplot.show()

def plot_sensitivity_compression():
    plot_num_of_tuple = [500, 1000, 2000, 5000, 10000]
    plot_corel = [0.03, 0.068, 0.077, 0.090, 0.102]
    plot_forest_cover = [0.015, 0.018, 0.048, 0.080, 0.086]

    pyplot.figure(figsize=(5,3))
    pyplot.semilogx(plot_num_of_tuple, plot_corel, '-o')
    pyplot.semilogx(plot_num_of_tuple, plot_forest_cover, '--s')
    pyplot.xlim(500, 10000)
    pyplot.xticks([500, 1000, 2000, 5000, 10000], [500, 1000, 2000, 5000, 10000])
    pyplot.legend(['Corel', 'Forest Cover'], prop = {'size':11}, loc = 2)
    pyplot.xlabel('# of Tuples for BN Learning', fontsize='large')
    pyplot.ylabel('Detected Correlation', fontsize='large')
    pyplot.tight_layout(pad=0);
    pyplot.show()

def plot_sensitivity_bn_edge():
    plot_num_of_tuple = [500, 1000, 2000, 5000, 10000]
    plot_corel = [10, 22, 26, 32, 40]
    plot_forest_cover = [9, 13, 28, 53, 64]

    pyplot.figure(figsize=(5,3))
    pyplot.semilogx(plot_num_of_tuple, plot_corel, '-o')
    pyplot.semilogx(plot_num_of_tuple, plot_forest_cover, '--s')
    pyplot.xlim(500, 10000)
    pyplot.xticks([500, 1000, 2000, 5000, 10000], [500, 1000, 2000, 5000, 10000])
    pyplot.xlabel('# of Tuples for BN Learning', fontsize='large')
    pyplot.ylabel('Detected BN Edges', fontsize='large')
    pyplot.legend(['Corel', 'Forest Cover'], prop = {'size':11}, loc = 2)
    pyplot.tight_layout(pad=0);
    pyplot.show()

def plot_compression_rate_lossless():
    bar_left_r = [0.8, 1.8]
    bar_left_y = [1, 2]
    bar_width = [0.2, 0.2]
    bar_height_y = [0.046, 0.0085]
    bar_height_r = [0.088, 0.0192]

    pyplot.figure(figsize=(5,3))
    p1 = pyplot.bar(bar_left_r, bar_height_r, bar_width, color='r')
    p2 = pyplot.bar(bar_left_y, bar_height_y, bar_width, color='y')
    pyplot.ylabel('Compression Ratio')
    pyplot.title('Compression Ratio Comparison')
    pyplot.xticks(bar_left_y, ('Census', 'Genomes'))
    pyplot.xlim(0.5, 2.5)
    pyplot.legend( (p2[0], p1[0]), ('Squish', 'Gzip'), loc = 1)
    pyplot.tight_layout(pad=0);
    pyplot.show()

def plot_compression_rate_categorical():
    bar_left_r = [0.7, 1.7]
    bar_left_y = [0.9, 1.9]
    bar_left_b = [1.1, 2.1]
    bar_width = [0.2, 0.2]
    bar_height_r = [0.140, 0.0629]
    bar_height_y = [0.090, 0.0171]
    bar_height_b = [0.046, 0.0085]

    pyplot.figure(figsize=(5,3))
    p1 = pyplot.bar(bar_left_r, bar_height_r, bar_width, color='r')
    p2 = pyplot.bar(bar_left_y, bar_height_y, bar_width, color='y')
    p3 = pyplot.bar(bar_left_b, bar_height_b, bar_width, color='b')
    pyplot.ylabel('Compression Ratio')
    pyplot.title('Categorical Compression Ratio')
    pyplot.xticks(range(1, 3), ('Census', 'Genomes'))
    pyplot.xlim(0.4, 2.6)
    pyplot.legend( (p1[0], p2[0], p3[0]), ('Domain', 'Column', 'Full'), loc = 1)
    pyplot.tight_layout(pad=0);
    pyplot.show()

def plot_compression_rate_numerical():
    bar_left = [0.8, 1.8, 2.8, 3.8, 4.8]
    bar_width = [0.4, 0.4, 0.4, 0.4, 0.4]
    bar_height = [0.435, 0.326, 0.223, 0.218, 0.109]

    pyplot.figure(figsize=(5,3))
    pyplot.bar(bar_left, bar_height, bar_width)
    pyplot.ylabel('Compression Ratio')
    pyplot.title('Numerical Compression Ratio')
    pyplot.xticks(range(1, 6), ('IEEE Float', 'Discrete', 'Column', 'Full', 'Lossy'))
    pyplot.xlim(0.3, 5.7)
    pyplot.tight_layout(pad=0);
    pyplot.show()
    
plot_compression_rate_forest_cover()
#plot_compression_rate_corel()
#plot_compression_rate_lossless()
#plot_compression_rate_categorical()
#plot_compression_rate_numerical()
#plot_sensitivity_compression()
#plot_sensitivity_bn_edge()
