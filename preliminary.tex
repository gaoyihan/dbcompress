%!TEX root=DBCompress.tex

\section{Preliminaries}\label{sec_prelim}
\noindent In this section, we define our problem more formally,
and provide some background on \techreport{Bayesian Networks
and }Arithmetic Coding.
\subsection{Problem Definition}\label{sec_prob_def}

We follow the same problem definition proposed by Babu et al.~\cite{babu2001spartan}.
Suppose our dataset consists of a single relational table $T$, with $n$ rows and $m$ columns (our techniques extend to multi-relational case as well). Each row of the table is referred to as a tuple and each column of the table is referred to as an attribute. We assume that each attribute has an associated domain that is known to us. For instance, this information could be described when the table schema is specified. 

The goal is to design a compression algorithm $A$ and a decompression algorithm $B$, such that $A$ takes $T$ as input and outputs a compressed file $C(T)$, and $B$ takes the compressed file $C(T)$ as input and outputs $T'$ as the approximate reconstruction of $T$. The goal is to minimize the file size of $C(T)$ while ensuring that the recovered $T'$ is close enough to $T$.

%\todo{diagram}

The closeness constraint of $T'$ to $T$ is defined as follows: For each numerical attribute $i$, for each tuple $t$ and the recovered tuple $t'$, $|t_i - t'_i| \leq \epsilon_i$, where $t_i$ and $t'_i$ are the values of attribute $i$ of tuple $t$ and $t'$ respectively, and $\epsilon_i$ is error threshold parameter provided by the user. For non-numerical attributes, the recovered attribute value must be exactly the same as the original one: $t_i = t'_i$.
Note that this definition subsumes lossless compression as a special case with $\epsilon_i = 0$. 

\techreport{
\subsection{Bayesian Network}\label{sec_bayesian}

Bayesian Networks~\cite{koller2009probabilistic} are widely used probabilistic models to describe the correlations between a collection of random variables. A Bayesian Network can be described using a directed acyclic graph among random variables, together with a set of conditional probability distributions for each random variable that only depends on the value of parent random variables in the graph (i.e., the neighbor nodes associated with incoming links). 

More formally, a Bayesian Network $\mathcal{B} = (\mathcal{G}, (\mathcal{M}_1, \ldots, \mathcal{M}_n))$ consists of two components: The structure graph $\mathcal{G} = (V, E)$ is a directed acyclic graph with $n$ vertices $v_1, \ldots, v_n$ corresponding to $n$ random variables (denote them as $X_1, \ldots, X_n$). For every random variable $X_i$, $\mathcal{M}_i$ is a model describing the conditional distribution of $X_i$ conditioned on $\mathbf{X}_{parent(i)} = \{X_j : j \in parent(i)\}$, where $parent(i) = \{j :  (v_j, v_i) \in E\}$ is called the set of parent nodes of $v_i$ in $\mathcal{G}$. 

Figure~\ref{fig_bn_example} shows an example Bayesian Network with three random variables $X_1, X_2, X_3$. The structure graph $\mathcal{G}$ is shown at top of the figure and the bottom side shows the models $\mathcal{M}_1, \mathcal{M}_2, \mathcal{M}_3$.

\begin{figure}[h]
	\centering
	\includegraphics[width = 7.5cm]{new_bn_example.png}
	\caption{Bayesian Network Example}\label{fig_bn_example}
\end{figure}

In this work, we will use Bayesian Networks to model the correlations between attributes within each tuple. In particular, the random variables will correspond to attributes in the dataset and edges will capture correlations. Thus, each tuple can be viewed as a joint-instantiation of values of the random variables in the Bayesian Network, or equivalently, a sample from the probability distribution described by the Bayesian Network.
}

\begin{figure*}[ht]
	\centering
	\vspace{-5pt}
	\includegraphics[width = 16cm]{arithmetic_coding_example.png}
	\vspace{-10pt}
	\caption{Arithmetic Coding Example}\label{fig_arithmetic_coding}
	\vspace{-10pt}
\end{figure*} % it is for the next section

\subsection{Arithmetic Coding}\label{sec_arithmetic}

Arithmetic coding~\cite{witten1987arithmetic,mackay2003information} is a state-of-the-art adaptive compression algorithm for a sequence of dependent characters. Arithmetic coding assumes as a given a conditional probability distribution model for any character, conditioned on all preceding characters. If the sequence of characters are indeed generated from the probabilistic model, then arithmetic coding can achieve a near-entropy compression rate~\cite{mackay2003information}.

Formally, arithmetic coding is defined by a finite ordered alphabet $\mathcal{A}$, and a probabilistic model for a sequence of characters that specifies the probability distribution of each character $X_k$ conditioned on all precedent characters $X_1, \ldots, X_{k-1}$. Let $\{a_n\}$ be any string of length $n$. To compute the encoded string for $\{a_n\}$, we first compute a probability interval for each character $a_k$:
\begin{align*} 
[l_k, r_k] = & [p(X_k < a_k|X_1 = a_1, \ldots, X_{k-1} = a_{k-1}), \\
			 & p(X_k \leq a_k|X_1 = a_1, \ldots, X_{k-1} = a_{k-1})]
\end{align*}

We define the product of two probability interval as:
$$[l_1, r_1] \circ [l_2, r_2] = [l_1 + (r_1 - l_1) l_2, l_1 + (r_1 - l_1) r_2]$$
% Intuitively, $[l_1, r_1] \circ [l_2, r_2]$ is a sub-interval of $[l_1, r_1]$, and $[l_2, r_2]$ describes the location of the sub-interval relative to $[l_1,r_2]$. 

The probability interval for string $\{a_n\}$ is the product of probability intervals of all the characters in the string:
$$ [l,r] = [l_1,r_1] \circ [l_2,r_2] \circ \ldots \circ [l_n, r_n] $$

Let $k$ be the smallest integer such that there exists a non-negative integer $0 \leq M < 2^k$ satisfying:
$$ l \leq 2^{-k}M, r \geq 2^{-k} (M + 1) $$
Then the $k$-bit binary representation of $M$ is the encoded bit string of $\{a_n\}$.

An example to illustrate how arithmetic coding works can be found in Figure~\ref{fig_arithmetic_coding}. The three tables at the right hand side specify the probability distribution of the string $a_1 a_2 a_3$. The blocks at the left hand show the associated probability intervals for the strings: for example, ``aba'' corresponds to $[0.12, 0.204] = [0, 0.4] \circ [0.3, 1] \circ [0, 0.3]$. As we can see, the intuition of arithmetic aoding is to map each possible string $\{a_n\}$ to disjoint probability intervals. By using these probability intervals to construct encoded strings, we can make sure that no code word is a prefix of another code word.

% \agp{Some more intuition would be helpful. Maybe one line describing what a, ab, and aba map to.}

Notice that the length of the product of probability intervals is exactly the product of their lengths. Therefore, the length of each probability interval is exactly the same as the probability of the corresponding string $\mathbf{Pr}(\{a_n\})$. Using this result, the length of encoded string can be bounded as follows:
$$ \textit{len}(\textit{binary\_code}(\{a_n\})) \leq \lfloor - \log_2 \mathbf{Pr}(\{a_n\}) \rfloor + 2 $$