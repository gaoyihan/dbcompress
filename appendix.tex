%\section*{Appendix}

\subsection*{Proof of Theorem~\ref{thm_numerical}}

\vspace{5pt}

\begin{proof}
	For any $x \in \mathcal{X}$, define $S(x)$ as follows:
	$$ S(x) = \{y \in \mathcal{X} : g(y) = g(x)\} $$
	Then the probability that $g(x)$ is the code word is:
	$$ \mathbf{Pr}(g(x)) = \int_{y \in S(x)} f(y) dy $$
	The entropy of $g(x)$ is:
	$$ H(g(x)) = E_X[- \log_2 \mathbf{Pr}(g(X))] $$
	Since $g(x)$ is uniquely decodable, the average length must be greater than or equal to the entropy:
	$$ E_X[len(g(X))] \geq H(g(x)) $$
	For the first part, since $- \log x$ is decreasing function, it suffices to prove that
	$$ \forall x \in \mathcal{X}, \mathbf{Pr}(g(x)) \leq (2 \rho(x) \epsilon + 1) 4 \epsilon f(x) $$
	Note that due to closeness constraint, we have
	$$ S(x) \subseteq [h(g(x)) - \epsilon, h(g(x)) + \epsilon]$$
	Combined with the fact that $|h(g(x)) - x| \leq \epsilon$, we have
	$$ S(x) \subseteq [x - 2\epsilon, x + 2\epsilon] $$
	and
	$$ \mathbf{Pr}(g(x)) = \int_{y \in S(x)} f(y) dy \leq \int_{x - 2\epsilon}^{x + 2\epsilon} f(y) dy $$
	The right hand side can be upper bounded by
	$$ \int_{x - 2\epsilon}^{x + 2\epsilon} f(y) dy \leq 4 \epsilon (2 \epsilon \rho(x) + 1) f(x) $$
	since by Equation~(\ref{eqn_self_bounded}),
	$$\forall y \in \mathcal{X}, |x - y| < 2\epsilon \Rightarrow f(y) \leq (2 \epsilon \rho(x) + 1) f(x)$$
	
	For the second part, by the property of Arithmetic Coding, we have
	$$ E_X[len(g(X))] \leq H(g(x)) + 2 $$
	We will prove
	\begin{equation}\label{eqn_aux}
	\forall x \in \mathcal{X}, \mathbf{Pr}(g(x)) \geq \min(\frac{l}{2}, \frac{1}{4 \rho(x)}) f(x)
	\end{equation}
	Let us suppose Equation~(\ref{eqn_aux}) is true for now, then we have
	\begin{align*}
	H(g(X)) = & E_X[- \log_2 \mathbf{Pr}(g(x))] \\
	\leq & E_X[- \log_2 \min(\frac{l}{2}, \frac{1}{4 \rho(x)}) f(x)] \\
	= & E_X[- \log_2 f(x)] - \\
	& \quad E_X[\log_2 l + \min(\log_2 \frac{1}{2 \rho(x)} - \log_2 l, 0)] + 1 \\
	\leq & E_X[- \log_2 f(x)] - \log_2 l + \\
	& \quad E_X[\max(\log_2 \rho(x) + \log_2 l, 0)] + 2
	\end{align*}
	
	To prove Equation~(\ref{eqn_aux}), let $v$ be the leaf node that $x$ corresponds to, we consider two cases:
	\begin{enumerate}
		\item
		If $v_r - v_l < \frac{1}{2 \rho(x)}$, then
		$$ \forall y \in [v_l, v_r], f(y) \geq (1 - (v_r - v_l) \rho(x)) f(x) \geq \frac{1}{2} f(x) $$
		thus we have
		$$ \mathbf{Pr}(g(x)) \geq \frac{1}{2} (v_r - v_l) f(x) \geq \frac{1}{2} l f(x) $$
		\item
		If $v_r - v_l \geq \frac{1}{2 \rho(x)}$, then
		$$ \forall y \in [x - \frac{1}{2 \rho(x)}, x + \frac{1}{2 \rho(x)}] \cap [v_l, v_r], f(y) \geq \frac{1}{2} f(x) $$
		Therefore,
		$$ \mathbf{Pr}(g(x)) \geq \frac{1}{2} f(x) len([v_l,v_r] \cap [x - \frac{1}{2 \rho(x)}, x + \frac{1}{2 \rho(x)}]) $$
		Since $x \in [v_l, v_r]$, $[x - \frac{1}{2 \rho(x)}, x + \frac{1}{2 \rho(x)}]$ covers at least $\frac{1}{2 \rho(x)}$ length of $[v_l, v_r]$, therefore,
		$$ \mathbf{Pr}(g(x)) \geq \frac{1}{4 \rho(x)} f(x)  $$
	\end{enumerate}
	Since $\mathbf{Pr}(g(x))$ is always greater than or equal to at least one of terms, it must always be greater than or equal to the minimum of two. Thus Equation~(\ref{eqn_aux}) is proved.
\end{proof}

\subsection*{Proof of Theorem~\ref{thm_decode}}

\vspace{5pt}

\begin{proof}
	First we briefly examine Algorithm~\ref{alg_encode}. Define $a_i, b_i, c_i$ as follows:
	\begin{align*} 
	& a_i = b_{i-1} \diamond [l_i, r_i] (b_0 = [0,1]) \\
	& b_i = [2^{k_i} a_i.l - M_i, 2^{k_i} a_i.r - M_i] \\
	& c_i = c_{i-1} \circ [2^{-k_i}M_i, 2^{-k_i}(M_i + 1)]
	\end{align*}
	where $k_i$ is the largest integer such that 
	$$a_i \subseteq [2^{-k_i} M_i, 2^{-k_i} (M_i + 1)]$$
	Then, we have the following results:
	\begin{itemize}
		\item
		$a_i$ is the value of $I_t$ after executing the first step of $i$th iteration of the loop, $b_i$ is the value of $I_t$ at the end of $i$th iteration of the loop,
		and $c_i$ is the probability interval corresponding to $\text{code}$.
		\item
		$s$ is the binary code of the probability interval $c_n \circ b_n$.
		
		Define $\text{PI}_s = [2^{-len(s)}s, 2^{-len(s)}(s + 1)]$, then 
		$$\text{PI}_s \subseteq c_n \circ b_n \subseteq c_i \circ b_i = c_{i-1} \circ a_i$$
	\end{itemize}
	
	We can prove the following statements by induction on the steps of Algorithm~\ref{alg_decode} (see Table~\ref{tbl_decode_example} for reference):
	\begin{itemize}
		\item
		During the procedure of determining $i$th branch, the value of $I_t$ is:
		\begin{itemize}
			\item
			$b_{i-1}$, during the first while loop.
			\item
			$a_i$, after executing the first statement inside the if block.
			\item
			$b_i$, at the end of the if block.
		\end{itemize}
		\item
		During the procedure of determining $i$th branch, let $d$ be the input that the algorithm has read in. Define
		$$ \text{PI}_d = [2^{-len(d)}d, 2^{-len(d)}(d + 1)] $$
		Then the value of $I_b$ satisfies:
		\begin{itemize}
			\item
			$\text{PI}_d = c_{i-1} \circ I_b$, during the first while loop.
			\item
			$\text{PI}_d = c_i \circ I_b$, at the end of the if block.
		\end{itemize}
		\item
		$ I_b \subseteq I_t $ always holds.
	\end{itemize}
	The induction step is easy for most parts, we will only prove the nontrivial part that the loop condition is satisfied before reaching the end of input. i.e., after certain steps we must have
	$$ I_b \subseteq I_t \diamond [l_i, r_i] $$
	To prove this, note that
	\begin{align*}
	\text{PI}_s \subseteq \text{PI}_d = c_{i-1} \circ I_b \\
	\text{PI}_s \subseteq c_{i-1} \circ a_i
	\end{align*}
	Therefore,
	$$(c_{i-1} \circ I_b) \cap (c_{i-1} \circ a_i) \neq \emptyset \Leftrightarrow I_b \cap a_i \neq \emptyset$$
	Note that $a_i = b_{i-1} \diamond [l_i, r_i] = I_t \diamond [l_i, r_i]$ during the while loop. Thus for any other branch $[l',r']$, $I_b \not\subseteq I_t \diamond [l',r']$ due to the fact that $[l', r'] \cap [l_i, r_i] = \emptyset$. Also note that as we continue reading new bits, $\text{PI}_d = c_{i-1} \circ I_b$ approaches $\text{PI}_s$, thus eventually we would have:
	$$ c_{i-1} \circ I_b \subseteq c_{i-1} \circ a_i \Leftrightarrow I_b \subseteq a_i $$
\end{proof}

\subsection*{Proof of Theorem~\ref{thm_asymptotic}}

\vspace{5pt}

\begin{proof}
	Since our compression algorithm searches for the Bayesian Network with minimum description length, it suffices to prove that the size of compressed dataset using the correct Bayesian Network is less than $H(\mathcal{D}) + 4n + 32m M^{c+1}$.
	
	Let $S$ be the set of tuples in $\mathcal{D}$ such that its probability is less than $2^{-n}$:
	$$ S = \{i \in [n] : P(t_i) < 2^{-n}\} $$
	Then, the size of the compressed dataset can be expressed as:
	\begin{align*}
	\text{compressed size} \leq & \sum_{i \in S} (- \log_2 P(t_i) - (\log_2 n - 2) + 2) \\
	& + 2 (n - |S|) + (\text{cost of model description})\\
	\leq & \sum_{i \in S} (- \log_2 P(t_i)) - |S| \log_2 |S| \\
	& + 4n + 32 m M^{c+1}
	\end{align*}
	where we assume that the model parameters are stored using single precision float numbers.
	
	On the other hand, since $S$ deterministically depends on $\mathcal{D}$, therefore by the chain rule of entropy we have:
	\begin{align*}
	& H(\mathcal{D}) = H(\mathcal{D}) + H(S|\mathcal{D}) = H(\mathcal{D}, S) \\
	= & H(S) + H(t_S|S) + H(t_{[n] \setminus S}|S) \geq H(t_S|S)
	\end{align*}
	and since $t_S$ is a multi-set consisting of $\{t_i : i \in S\}$, we have
	$$ H(t_S|S) \geq - |S| E[\log_2 P(t) | P(t) < 2^{-n}] - |S| \log_2 |S| $$	
	
	Combining these two directions, we conclude that
	$$ \text{compressed size} \leq H(\mathcal{D}) + 4n + 32m M^{c+1} $$
\end{proof}

