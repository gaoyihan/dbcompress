%!TEX root=DBCompress.tex

\techreport{
\section{Discussion: Examples and Optimality}\label{sec_optimality}

In this section, we use three example types of datasets to illustrate
how our compression algorithm can effectively find compact representations.
These examples also demonstrates the wide applicability of the \system\ approach.
We then describe the overall optimality of our algorithm.

\subsection{Illustrative Examples}\label{sec_example_dataset}

We now describe three types of datasets in turn.

\vspace{2mm}
\noindent \textbf{Pairwise Dependent Attributes}
\vspace{2mm}

Consider a dataset with 100 binary attributes $a_1, \ldots,$ $a_{100}$, where $a_1, \ldots, a_{50}$ are independent and uniformly distributed, and $a_{51}, \ldots,$ $ a_{100}$ are identical copies of $a_1, \ldots, a_{50}$ (i.e., $a_{i+50} = a_i$). 

Let us consider a tuple $(x_1, x_2, \ldots, x_{100})$. The probability intervals for the first $50$ attributes are $[\frac{x_i}{2}, \frac{x_i + 1}{2}]$ (i.e., $[0, \frac{1}{2}]$ if $x_i = 0$ and $[\frac{1}{2}, 1]$ otherwise). For the last $50$ attributes, since they deterministically depend on the first $50$ attributes, the probability interval is always $[0, 1]$. Therefore the probability interval for the whole tuple is:
$$ [\frac{x_1}{2}, \frac{x_1 + 1}{2}] \circ \ldots \circ [\frac{x_{50}}{2}, \frac{x_{50} + 1}{2}] \circ [0,1] \circ \ldots \circ [0,1] $$
It is easy to verify that the binary code string of the tuple is exactly the $50$-bits binary string $x_1 x_2\ldots x_{50}$ (recall that each $x_i$ is either $0$ or $1$). 

We also need to store the model information, which consists of the probability distribution of $x_1, \ldots, x_{50}$, the parent node of $x_{51}, \ldots, x_{100}$ and the conditional probability distribution of $x_{i+50}$ conditioned on $x_i$.

Assuming all the model parameters are stored using an $8$-bit code, then the model can be described using $200 \times 8 = 1600$ bits. Since each tuple uses $50$ bits, in total \system~would use $1600 + 50n$ bits for the whole dataset, where $n$ is the number of tuples. Note that our compression algorithm achieves much better compression rate than Huffman Coding~\cite{huffman1952method}, which uses at least $100$ bits per tuple.

Dependent attributes exist in many datasets. While they are usually only softly dependent (i.e., one attribute influences but does not completely determine the other attribute), our algorithm can still exploit these dependencies in compression.

\vspace{2mm}
\noindent \textbf{Markov Chain}
\vspace{2mm}

Figure~\ref{fig_markov} shows an example dataset with 1000 categorical attributes, in which each attribute $a_i$ depends on the preceding attribute $a_{i-1}$, and $a_1$ is uniformly distributed. This kind of dependency is called a {\em Markov Chain}, and frequently occurs in time-series datasets.

\begin{figure}[h]
	\centering
	\vspace{-5pt}	
	\includegraphics[width = 6cm]{markov_example.png}
	\vspace{-5pt}	
	\caption{Markov Chain Example}\label{fig_markov}
	\vspace{-5pt}	
\end{figure}

The probability interval of tuple $t = (x_1, \ldots, x_{1000})$ is:
$$ [\frac{x_1}{4}, \frac{x_1 + 1}{4}] \circ g(x_1, x_2) \circ g(x_2, x_3) \circ \ldots \circ g(x_{999}, x_{1000}) $$
where mapping $g(x,y)$ is listed below:
\begin{center}
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		& y=1 & y=2 & y=3 & y=4 \\
		\hline
		x=1 & $[0,2/3]$ & $[2/3, 7/9]$ & $[7/9, 8/9]$ & $[8/9, 1]$ \\
		\hline
		x=2 & $[0,1/9]$ & $[1/9, 7/9]$ & $[7/9, 8/9]$ & $[8/9, 1]$ \\
		\hline
		x=3 & $[0,1/9]$ & $[1/9, 2/9]$ & $[2/9, 8/9]$ & $[8/9, 1]$ \\
		\hline
		x=4 & $[0,1/9]$ & $[1/9, 2/9]$ & $[2/9, 1/3]$ & $[1/3, 1]$ \\
		\hline
	\end{tabular}
\end{center}

On average, for each tuple our algorithm uses about
$$ 1000 \times (\frac{2}{3} \log_2 \frac{3}{2} + 3 \times \frac{1}{9} \log_2 9) \approx 1443 \text{ bits}$$
while standard Huffman Coding~\cite{huffman1952method} uses $2000$ bits.

Time series datasets usually contain a lot of redundancy that can be used to achieve significant compression. As an example, most electrocardiography (ECG) waveforms~\cite{jalaleddine1990ecg} can be restored using a little extra information if we know the cardiac cycle. Our algorithm offers effective ways to utilize such redundancies for compression.

\vspace{2mm}
\noindent \textbf{Clustered Tuples}
\vspace{2mm}

Figure~\ref{fig_cluster} shows an example with 100 binary attributes: $a_1, \ldots, a_{100}$. In this example, $c$ is the hidden cluster index attribute and all other attributes are dependent on it.

\begin{figure}[h]
	\centering
	\vspace{-5pt}	
	\includegraphics[width = 5cm]{cluster_example.png}
	\vspace{-5pt}	
	\caption{Clustered Tuples Example}\label{fig_cluster}
	\vspace{-5pt}	
\end{figure}

If we compress the cluster index together with all attributes using our algorithm, we will need about
$$ H(t_i) = 1 + 100 \times (0.2 \log_2 \frac{1}{0.2} + 0.8 \log_2 \frac{1}{0.8}) \approx 73\text{ bits} $$
for each tuple. Note that it is less than the $100$-bits used by the plain binary code.

Many real datasets have a clustering property. To compute the cluster index, we need to choose an existing clustering algorithm that is most appropriate to the dataset at hand~\cite{jain1999data}. The extra cost of storing a cluster index is usually small compared to the overall saving in compression.

\subsection{Asymptotic Optimality}
}
\paper{
\section{Discussion: Optimality}\label{sec_optimality}
}

We can prove that \system~achieves asymptotic near-optimal compression rate for lossless compression if the dataset only contains categorical attributes and can be described efficiently using a Bayesian network (the proof can be found in\paper{~\cite{techreport}}\techreport {the appendix}):

\begin{theorem}\label{thm_asymptotic}
	Let $a_1, a_2, \ldots, a_m$ be categorical attributes with joint probability distribution $P(a_1, \ldots, a_m)$ that decomposes as
	$$ P(a_1, \ldots, a_m) = \prod_{i=1}^m P(a_i|\text{parent}_i) $$
	such that
	$$ \text{parent}_i \subseteq \{a_1, \ldots, a_{i-1}\}, \text{card}(\text{parent}_i) \leq c $$
	
	Suppose the dataset $\mathcal{D}$ contains $n$ tuples that are i.i.d. samples from $P$. Let $M = \max_i \text{card}(a_i)$ be the maximum cardinality of attribute range. Then \system~can compress $\mathcal{D}$ using less than $H(\mathcal{D}) + 4n + 32 m M^{c+1}$ bits on average, where $H(\mathcal{D})$ is the entropy~\cite{cover2012elements} of the dataset $\mathcal{D}$.
\end{theorem}

Thus, when $n$ is large, the difference between the size of the compressed dataset using our system and the entropy\footnote{By  Shannon's source coding theorem~\cite{cover2012elements}, there is no algorithm that can achieve compression rate higher than entropy.} of $\mathcal{D}$ is at most $5n$, that is only $5$ bits per tuple. This indicates that \system~is asymptotically near-optimal for this setting.

When the dataset $\mathcal{D}$ contains numerical attributes, the entropy $H(\mathcal{D})$ is not defined, and the techniques we used to prove Theorem~\ref{thm_asymptotic} no longer apply. However, in light of Theorem~\ref{thm_numerical}, it is likely that \system~still achieves asymptotic near-optimal compression. 
