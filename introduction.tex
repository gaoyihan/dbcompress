%!TEX root=DBCompress.tex

\section{Introduction}

From social media interactions, commercial transactions, to scientific observations and the internet-of-things, 
relational datasets are being generated at an alarming rate. With these datasets, that are {\em either costly or impossible
to regenerate, there is a need for periodic archival,} for a variety of purposes, including long-term analysis or machine learning, 
historical or legal factors, or public or private access over a network. Thus, despite the declining costs of storage, 
compression of relational datasets is still important, and will stay important in the coming future.

One may wonder if compression of datasets is a solved problem. Indeed, there has been a variety of robust algorithms like Lempel-Ziv~\cite{ziv1977universal},
WAH~\cite{wu2004word}, and CTW~\cite{willems1995context} developed and used widely for data compression. However, these algorithms do not exploit the relational
structure of the datasets: attributes are often correlated or dependent on each other, and identifying and exploiting such correlations can lead to 
significant reductions in storage. In fact, there are many types of dependencies between attributes in a relational dataset.
For example, attributes could be functionally dependent on other attributes~\cite{babu2001spartan}, or the dataset could consist of several clusters of tuples, such that all the tuples within each cluster are similar to each other~\cite{jagadish2004itcompress}. The skewness of numerical attributes is another important source of redundancy that is overlooked by algorithms like Lempel-Ziv~\cite{ziv1977universal}: by designing encoding schemes based on the distribution of attributes, we can achieve much better compression rate than storing the attributes using binary/float number format.

There has been some limited work on compression of relational datasets, all in the recent past~\cite{babu2001spartan, davies1999bayesian, jagadish2004itcompress, raman2006wring}. In contrast with this line of prior work, \system~uses a combination of Bayesian Networks coupled with Arithmetic Coding~\cite{witten1987arithmetic}. 
Arithmetic Coding is a coding scheme designed for sequence of characters.
It requires an order among characters and probability distributions of characters conditioned on all preceding ones.
Incidentally, Bayesian Networks fulfill both requirements: 
the acyclic property of Bayesian Network provides us an order (i.e., the topological order), 
and the conditional probability distributions are also specified in the model. 
Therefore, Bayesian Networks and Arithmetic Coding are a perfect fit for relational dataset compression.
% \agp{We need to identify why this combination is a challenging problem:	what are the novel aspects in the combination.}

% \todo{one paragraph, challenges}
However, there are several challenges in using Bayesian Networks and Arithmetic Coding for compression. First, we need to identify a new objective function for learning a Bayesian Network, since conventional objectives like Bayesian Information Criterion~\cite{schwarz1978estimating} are not designed to minimize the size of the compressed dataset. Another challenge is to design a mechanism to support attributes with an infinite range (e.g., numerical and string attributes), since Arithmetic Coding assumes a finite alphabet for symbols, and therefore cannot be applied to those attributes. To be applicable to the wide variety of real-world datasets, it is essential to be able to handle numbers and strings.

We deal with these challenges in developing \system. As we show in this paper, the compression rate of \system~is near-optimal for all datasets that can be efficiently described using a Bayesian Network.
This theoretical optimality reflects in our experiments as well: {\em \system~achieves a reduction in storage on real datasets of over 50\% compared to the nearest competitor. } 
The reason behind this significant improvement is that most prior papers use sub-optimal techniques for compression.
% \todo{one sentence, why prior work is suboptimal}

In addition to being more effective at compression of relational datasets than prior work, \system~is also more {\em powerful}. To demonstrate that, 
we identify the following desiderata for a relational dataset compression system:
\begin{denselist}
	\item {\em Attribute Correlations (AC).} 
	A relational dataset compression system must be able to capture correlations across attributes.
	\item {\em Lossless and Lossy Compression (LC).} 
	A relational dataset compression system must be general enough to admit a user-specified error tolerance, 
	and be able to generate a compressed dataset in a lossy fashion, while respecting the error tolerance, further saving on storage.
	\item {\em Numerical Attributes (NA).} 
	In addition to string and categorical attributes, a relational dataset compression system must be able to capture numerical attributes, 
	that are especially	common in scientific datasets.
	\item {\em User-defined Attributes (UDA).} 
	A relational dataset compression system must be able to admit new types of attributes that do not fit into 
	either string, numerical, or categorical types.
\end{denselist}

In contrast to prior work~\cite{babu2001spartan, davies1999bayesian, jagadish2004itcompress, raman2006wring}---see Table~\ref{tbl_intro}---our system, \system, can capture all of these desiderata. To support UDA (User Defined Attributes),
\system~surfaces a new class interface, called the {\sc SquID} (short for \system~Interface for Data types): 
users can instantiate new data types by simply implementing the required five functions. 
This interface is remarkably powerful, especially for datasets in specialized domains.
For example, a new data type corresponding to genome sequence data can be implemented by a user using a few hundred lines of code. By encoding domain knowledge into the data type definition, we can achieve a significant higher compression rate than using ``universal'' compression algorithms like Lempel-Ziv~\cite{ziv1977universal}.
%\agp{Can we say anything else about what this benefits?}

\begin{table}[h]
	\centering
	\vspace{-5pt}
	\begin{tabular}{c|c|c|c|c}
		System & AC & NA & LC & UDA \\
		\hline \hline
		\system & Y & Y & Y & Y \\
		\hline
		Spartan~\cite{babu2001spartan} & Y & Y & Y & N \\
		\hline
		ItCompress~\cite{jagadish2004itcompress} & Y & Y & Y & N \\
		\hline
		Davies and Moore~\cite{davies1999bayesian} & Y & N & N & N \\
		\hline
		Raman and Swart~\cite{raman2006wring} & N & N & N & N \\
	\end{tabular}
			\vspace{-5pt}
		\caption{Features of Our System contrasted with Prior Work}\label{tbl_intro}
		\vspace{-5pt}
\end{table}

The rest of this paper is organized as follows. In Section~\ref{sec_prelim}, we formally define the problem of relational dataset compression, and briefly explain the concepts of Arithmetic Coding. In Section~\ref{sec_algorithm} we discuss Bayesian Network learning and related issues, while details about Arithmetic Coding are discussed in Section~\ref{sec_implementation}. In Section~\ref{sec_optimality}, we \techreport{use examples to illustrate the effectiveness of \system~and }prove the asymptotic optimality of the compression algorithm. In Section~\ref{sec_experiment}, we conduct experiments to compare \system~with prior systems and evaluate its running time and parameter sensitivity. We describe related work in Section~\ref{sec_related_work}. All our proofs can be found \techreport{in the appendix}\paper{in our technical report~\cite{techreport}, along with a brief review of Bayesian Networks and illustrative examples of our compression algorithm}.

The source code of \system~is available on GitHub:  https://gith ub.com/Preparation-Publication-BD2K/db\_compress
%\agp{Fix this if you change the order of the sections. Also mention what is in the appendix.}

% \todo{conclusion?}

%The major contributions of this paper are:
%\begin{itemize}
%	\item
%	We identify the correct objective function for learning the optimal Bayesian Network, and provides a model-independent decomposition of the score to enable comparisons between arbitrary models.
%	\item
%	We develop mechanisms for supporting non-categorical and user-defined attributes, and characterize its connection to lossy compression setting.
%	\item
%	We provide a precision-aware implementation of arithmetic coding and prove its correctness.
%	\item
%	We implement an end-to-end system for compressing relational datasets, and experimentally compare it with other compression algorithms. The result shows that our algorithm can compress the dataset to 40\% less space than the state-of-the-art compression algorithm~\cite{babu2001spartan, jagadish2004itcompress} on average.
%\end{itemize}

%Given a relational database, it is usually possible to store it using much less space if we compress the database first. There are many benefits of compressing a database:
%\begin{itemize}
%	\item
%	By compressing a database, we can reduce the total storage space for large datasets, and thereby reduce the monetary cost for storing them.
%	\item
%	By transmitting a compressed dataset and decompress it on the receiver side, we can reduce the size of transmitted file and effectively improve the transmission rate.
%\end{itemize} 

%We study the problem of compressing a single table (relation) in a relational database. Comparing to syntactic compression algorithms like Lempel-Ziv~\cite{ziv1977universal}, algorithms designed to compress tables can exploit correlations between attributes (i.e., columns). Intuitively, if an attribute is dependent on another attribute, then we can store them together using less space.

%There are many types of dependencies between attributes.  To make our algorithm widely applicable, we capture the dependencies among attributes using Bayesian Network~\cite{koller2009probabilistic}, which is a general purpose probabilistic model describing the conditional probability distributions of attributes conditioned on other attributes. Arithmetic Coding~\cite{witten1987arithmetic}, an adaptive compression scheme that achieves near-entropy compression ratio, is used in our algorithm to compress a database based on a Bayesian Network.

%Two other aspects of the compression problem are studied in this paper: Lossy compression and user-defined attribute types. 
%To support user-defined attributes, we introduce a concept called Probability Tree, which uses a tree structure to define a probability distribution over all possible attribute values. Probability Tree is an interface with some undefined functions that require the user to implement. 
%Once the user implements the Probability Tree for an user-defined attribute, the system will do the rest of the works (compression and decompression) automatically.  We discuss example Probability Trees for numerical and string attributes in the paper to illustrate the wide applicability of this interface.

%Lossy compression can also be achieved using Probability Trees: since each leaf node of a Probability Tree corresponds to a subset of attribute values, we can control the maximum error resulted from compression by controlling the granularity of the Probability Tree (i.e, the range of attribute values for leaf nodes).

%The problem of compressing a table in a relational database has been studied in several works in literature. We will experimentally demonstrate the superiority of our approach using two benchmark datasets. In addition, our system supports features that are not supported by previous systems. Table~\ref{tbl_intro} lists the difference of the supported features between our system and some past works: whether the system exploits Attribute Correlation(AC) during compression, whether the system supports Numerical Attributes(NA), Lossy Compression(LC), and User-Defined Attributes(UDA). 
